import * as firebase from 'firebase';
require("firebase/firestore");

var collectionMeta={
	 
	"athletes":{
		"fields":{
			"Name":"Athletes Name",
			"Address":"Athletes Address",
			"Contact Name":"Contact Name",
			"Email":"Contact Email",
			"Mobile Number":"Athletes Mobile Number",
			"Payment Due":"Payment Due From Club",
			"Payment Due Date":"Last Date For Payment"
		},
		"collections":[]
	}

}
module.exports = collectionMeta;
