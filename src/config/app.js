//FireBase
exports.firebaseConfig = {
    apiKey: "AIzaSyCCpIwAIqLVFsgzXUMMubBcd2Gb1RaD5fo",
    authDomain: "kreed-of-sports.firebaseapp.com",
    databaseURL: "https://kreed-of-sports.firebaseio.com",
    projectId: "kreed-of-sports",
    storageBucket: "kreed-of-sports.appspot.com",
    messagingSenderId: "142735995489"
  };


//App setup
exports.adminConfig={
  "appName": "KreedOfSports",
  "slogan":"Powered by",
  "design":{
    "sidebarBg":"sidebar-5.jpg", //sidebar-1, sidebar-2, sidebar-3
    "dataActiveColor":"rose", //"purple | blue | green | orange | red | rose"
    "dataBackgroundColor":"black", // "white | black"
  },
  "showItemIDs":false,
  "allowedUsers":null, //If null, allow all users, else it should be array of allowd users
  "allowGoogleAuth":false, //Allowed users must contain list of allowed users in order to use google auth
  "fieldBoxName": "Fields",
  "maxNumberOfTableHeaders":5,
  "prefixForJoin":["-event"],
  "showSearchInTables":true,
  "showSearchInDocsTables":false,
  "methodOfInsertingNewObjects":"push", //timestamp (key+time) | push - use firebase keys
  "urlSeparator":"+",
  "urlSeparatorFirestoreSubArray":"~",
  //"googleMapsAPIKey":"YOUR_KEY",

  "fieldsTypes":{
    "photo":["photo","image"],
    "dateTime":["end","start","date_of_birth"],
    "map":["map","latlng","location"],
    "textarea":["description"],
    "html":["content"],
    "radio":["radio","radiotf","featured"],
    "checkbox":["checkbox"],
    "dropdowns":["status","dropdowns","blood_group"],
    "coach_name":["coach_name"],
    "file":["video"],
    "rgbaColor":['rgba'],
    "hexColor":['color'],
    "relation":['type','creator'],
    "iconmd":['icon'],
    "iconfa":['iconfa'],
    "iconti":['iconti'],
  },
  "optionsForDateTime":[
    {"key":"end", "dateFormat":"YYYY-MM-DD" ,"timeFormat":true, "saveAs":"x","locale":"es"},
    {"key":"start", "dateFormat":"YYYY-MM-DD" ,"timeFormat":"HH:mm", "saveAs":"YYYY-MM-DD HH:mm"}
  ],
  "optionsForSelect":[
      {"key":"coach_name","options":["clubCoaches"]},
      {"key":"blood_group","options":["A+","A-","B+","B-","AB+","AB-","O+","O-"]},
      {"key":"dropdowns","options":["new","processing","rejected","completed"]},
      {"key":"checkbox","options":["Skopje","Belgrade","New York"]},
      {"key":"status","options":["just_created","confirmed","canceled"]},
      {"key":"radio","options":["no","maybe","yes"]},
      {"key":"radiotf","options":["true","false"]},
      {"key":"featured","options":["true","false"]}
  ],
  "optionsForRelation":[
      {
        //Firestore - Native
        "display": "name",
        "isValuePath": true,
        "key": "creator",
        "path": "/users",
        "produceRelationKey": false,
        "relationJoiner": "-",
        "relationKey": "type_eventid",
        "value": "name"
      },
      {
        //Firebase - Mimic function
        "display":"name",
        "key":"eventtype",
        "path":"",
        "isValuePath":false,
        "value":"name",
        "produceRelationKey":true,
        "relationJoiner":"-",
        "relationKey":"type_eventid"
      }
  ],
  "paging":{
    "pageSize": 20,
    "finite": true,
    "retainLastPage": false
  }
}

//Navigation
exports.navigation=[
    {
      "link": "/",
      "name": "Dashboard",
      "schema":null,
      "icon":"dashboard",
      "path": "",
       isIndex:true,
    },
    {
      "link": "CoachAdmin",
      "path": "coaches",
      "name": "Coaches",
      "icon":"person",
      "tableFields":["photo","name","KSA_ID","email","mobile_number"],
      "editFields":["name","date_of_birth","blood_group","email","mobile_number","address","city","pin_code","country","photo"],
      "subMenus":[]
    },
    {
      "link": "AthleteAdmin",
      "path": "athletes",
      "name": "Athletes",
      "icon":"pool",
      "tableFields":["photo", "full_name", "KSA_ID", "email", "mobile_number"],
      "editFields":["full_name","date_of_birth","blood_group","email","mobile_number","address","city","pin_code","country","coach_name","photo"],
      "subMenus":[]
    },
    {
      "link": "MeetAdmin",
      "path": "meets",
      "name": "Meets",
      "icon":"alarm",
      "tableFields":["photo","KSA_ID","full_name","mobile_number","Registered_events"],
      // "tableFields":["category","distance","stroke","swim_group","event_number"],
      "editFields":["name","date_of_birth","blood_group","address","city","pin_code","country","email","mobile_number","photo"],
      "subMenus":[]
    },
    {
      "link": "ChangePassword",
      "path": "change_password",
      "name": "Change Password",
      "icon": "lock",
      "tableFields":["category","distance","stroke","swim_group","event_number"],
      "editFields":["name","date_of_birth","blood_group","address","city","pin_code","country","email","mobile_number","photo"],
      "subMenus":[]
    }
  ];

exports.pushSettings={
  "pushType":"expo", //firebase -  onesignal - expo
  "Firebase_AuthorizationPushKey":"AIzaSyCFUf7fspu61J9YsWE-2A-vI9of1ihtSiE", //Firebase push authorization ket
  "pushTopic":"news", //Only for firebase push
  "oneSignal_REST_API_KEY":"",
  "oneSignal_APP_KEY":"",
  "included_segments":"Active Users", //Only for onesignal push
  "firebasePathToTokens":"/expoPushTokens", //we save expo push tokens in firebase db
  "saveNotificationInFireStore":true, //Should we store the notification in firestore
}

exports.userDetails={

}

exports.remoteSetup=false;
exports.remotePath="admins/mobidonia";
exports.allowSubDomainControl=false;
exports.subDomainControlHolder="admins/";
