import React, {Component,PropTypes} from 'react'
class FlatIndicator extends Component {
  constructor(props){
    super(props)
  }


  render() {
    if(this.props.show){
      return (<img style={{width:80,height:80}} src={'../../assets/img/'+'animated-spin.gif'} />)
    }else{
      return (<div></div>)
    }
  }
}
export default FlatIndicator;
