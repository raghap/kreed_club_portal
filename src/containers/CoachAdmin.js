import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router'
import firebase from '../config/database'
import Fields from '../components/fields/Fields.js'
import Radio from '../components/fields/Radio.js'
import File from '../components/fields/File.js'
import Input from '../components/fields/Input.js';
import Table from '../components/tables/Table.js'
import Config from '../config/app';
import Common from '../common.js';
import Notification from '../components/Notification';
import SkyLight from 'react-skylight';
import INSERT_STRUCTURE from "../config/firestoreschema.js"
import FirebasePaginator from "firebase-paginator"
import NavBar from '../components/NavBar'
import moment from 'moment';
import Image from '../components/fields/Image.js';
import KreedImage from '../components/fields/KreedImage.js';
import * as firebaseREF from 'firebase';
import KreedDatePicker from '../components/fields/KreedDatePicker.js';
import Select from '../components/fields/Select.js';
import SweetAlert from 'react-bootstrap-sweetalert';
//import trim from 'trim';
require("firebase/firestore");

const ROUTER_PATH = "/coaches/";
var Loader = require('halogen/PulseLoader');

class CoachAdmin extends Component {

  constructor(props) {
    super(props);


    //Create initial step
    this.state = {
      fields: {}, //The editable fields, textboxes, checkbox, img upload etc..
      arrays: {}, //The array of data
      elements: [], //The elements - objects to present
      elementsInArray: [], //The elements put in array
      directValue: "", //Direct access to the value of the current path, when the value is string
      firebasePath: "",
      arrayNames: [],
      currentMenu: {},
      completePath: "",
      lastSub: "",
      isJustArray: false,
      currentInsertStructure: null,
      notifications: [],
      lastPathItem: "",
      pathToDelete: null,
      isItArrayItemToDelete: false,
      page: 1,
      documents: [],
      collections: [],
      currentCollectionName: "",
      isCollection: false,
      isDocument: false,
      keyToDelete: null,
      theSubLink: null,
      fieldsOfOnsert: null,
      isLoading: false,
      showAddCollection: "test",
      debounce: false,
      userinfo: [],
      imageLoading: false,
      full_name: "",
      gender: "",
      date_of_birth: this.startDOB(),
      blood_group: "O+",
      address: "",
      city: "Bangalore",
      pin_code: "",
      country: "India",
      mobile_number: "",
      email: "",
      photo: "https://firebasestorage.googleapis.com/v0/b/kreed-of-sports.appspot.com/o/photos%2Fdefault_profile_350x400.png?alt=media&token=0741bfbb-8f54-478e-9339-32e520d66c92",
      editedFields: [],
      errors: {},

      photoAlert: false,
      phoneAlert: false,
      loadingAlert: false,
      deleteAlert: false,
      deleteAthleteAlert: false
    };

    //Bind function to this

    this.getCollectionDataFromFireStore = this.getCollectionDataFromFireStore.bind(this);
    this.resetDataFunction = this.resetDataFunction.bind(this);
    this.processRecords = this.processRecords.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.cancelDelete = this.cancelDelete.bind(this);
    this.cancelAddFirstItem = this.cancelAddFirstItem.bind(this);
    this.doDelete = this.doDelete.bind(this);
    this.deleteFieldAction = this.deleteFieldAction.bind(this);
    this.refreshDataAndHideNotification = this.refreshDataAndHideNotification.bind(this);
    this.addKey = this.addKey.bind(this);
    this.showSubItems = this.showSubItems.bind(this);
    this.updatePartOfObject = this.updatePartOfObject.bind(this);
    this.addDocumentToCollection = this.addDocumentToCollection.bind(this);
    this.addItemToArray = this.addItemToArray.bind(this);
    this.addNewItem = this.addNewItem.bind(this);
    this.formValueCapture = this.formValueCapture.bind(this);
    this.ionViewDidLoad = this.ionViewDidLoad.bind(this);
    this.cancelAddNewItem = this.cancelAddNewItem.bind(this);
    this.saveEdits = this.saveEdits.bind(this);
    this.resetEdits = this.resetEdits.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.markUploaderStart = this.markUploaderStart.bind(this);
    this.apply = this.apply.bind(this);
    this.cancelphotoGuidelines = this.cancelphotoGuidelines.bind(this);
    this.validateInputs = this.validateInputs.bind(this);
    this.validatePhoto = this.validatePhoto.bind(this);

    //Alerts related
    this.hideAlert = this.hideAlert.bind(this);
    this.showAlert = this.showAlert.bind(this);
  }

  startDOB() {

    return new Date(new Date().setFullYear(new Date().getFullYear() - 5));
  }
  //sweet alerts for delete data
  showAlert(name) {
    this.setState({ [name]: true });
  }
  hideAlert(name) {
    this.setState({ [name]: false });
  }


  cancelphotoGuidelines() {
    this.refs.photoGuidelines.hide();
  }

  apply(file) {
    var src = window.URL.createObjectURL(file);
  }


  ionViewDidLoad() {
    this.userId = firebase.auth().currentUser.uid
    alert(this.userId);
  }

  //ionViewDidLoad();
  /**
   * Step 0a
   * Start getting data
   */
  componentDidMount() {
    //this.findFirestorePath();
    this.getMeTheFirestorePath();
    window.sidebarInit();

  }

  /**
  * Step 0b
  * Resets data function
  */
  resetDataFunction() {
    var newState = {};
    newState.documents = [];
    newState.collections = [];
    newState.currentCollectionName = "";
    newState.fieldsAsArray = [];
    newState.arrayNames = [];
    newState.fields = [];
    newState.arrays = [];
    newState.elements = [];
    newState.elementsInArray = [];
    newState.theSubLink = null;

    newState.full_name = "";
    newState.gender = "";
    newState.date_of_birth = this.startDOB();
    newState.blood_group = "O+";
    newState.address = "";
    newState.email = "";
    newState.mobile_number = "";
    newState.photo = "https://firebasestorage.googleapis.com/v0/b/kreed-of-sports.appspot.com/o/photos%2Fdefault_profile_350x400.png?alt=media&token=0741bfbb-8f54-478e-9339-32e520d66c92";
    newState.country = "India";
    newState.pin_code = "";
    newState.city = "Bangalore";
    newState.imageLoading = false;
    newState.editedFields = [];

    //newState.editedFields = [];
    this.resetEdits();

    this.setState(newState);
    //this.findFirestorePath();
    this.getMeTheFirestorePath();
  }

  /**
   * Step 0c
   * componentWillReceiveProps event of React, fires when component is mounted and ready to display
   * Start connection to firebase
   */
  componentWillReceiveProps(nextProps, nextState) {
    console.log("Next SUB: " + nextProps.params.sub);
    console.log("Prev SUB : " + this.props.params.sub);
    if (nextProps.params.sub == this.props.params.sub) {
      console.log("update now");
      this.setState({ isLoading: true })
      this.resetDataFunction();
    }
  }

  /**
   * Step 0d
   * getMeTheFirestorePath created firestore path based on the router parh
   */
  getMeTheFirestorePath() {
    this.userId = firebase.app.auth().currentUser.uid;
    var db = firebase.app.firestore();
    var docRef = db.collection("clubmaps").doc("user" + this.userId);
    docRef.get().then(doc => {
      if (doc.exists) {
        var pathdoc = doc.data();
        this.findFirestorePath(pathdoc.dbpath);
      }
      else { alert("error"); }
    })

    // var thePath=(this.props.route.path.replace(ROUTER_PATH,"").replace(":sub",""))+(this.props.params&&this.props.params.sub?this.props.params.sub:"").replace(/\+/g,"/");;
    // return thePath;
  }

  /**
   * Step 1
   * Finds out the Firestore path
   * Also creates the path that will be used to access the insert
   */
  findFirestorePath(firebasePath) {
    var pathData = {}
    if (this.props.params && this.props.params.sub) {
      pathData.lastSub = this.props.params.sub;
    }

    //Find the firestore path
    //var firebasePath= this.getMeTheFirestorePath();
    pathData.firebasePath = firebasePath + "/" + pathData.lastSub;
    pathData.firebasePath = pathData.firebasePath.replace(/\+/g, "/");

    pathData.lastSub = pathData.firebasePath.replace(/\//g, "+");

    //Find last path - the last item
    var subPath = pathData.lastSub;//this.props.params && this.props.params.sub ? this.props.params.sub : ""
    var items = subPath.split(Config.adminConfig.urlSeparator);
    pathData.lastPathItem = Common.capitalizeFirstLetter(items[items.length - 1]);
    pathData.completePath = subPath;

    //Save this in state
    this.setState(pathData);

    //Go to next step of finding the collection data
    this.getCollectionDataFromFireStore(pathData.firebasePath);
    // this.getCollectionDataFromFireStore(firebasePath);
  }

  /**
  * Step 2
  * Connect to firestore to get the current item we need
  * @param {String} collection - this infact can be collection or document
  */
  getCollectionDataFromFireStore(collection) {

    //Create the segmments based on the path / collection we have
    var segments = collection.split("/");
    var lastSegment = segments[segments.length - 1];

    //Is this a call to a collections data
    var isCollection = segments.length % 2;

    //Reference to this
    var _this = this;

    //Save know info for now
    this.setState({
      currentCollectionName: segments[segments.length - 1],
      isCollection: isCollection,
      isDocument: !isCollection,
    })

    //Get reference to firestore
    var db = firebase.app.firestore();

    //Here, we will save the documents from collection
    var documents = [];

    if (isCollection) {
      //COLLECTIONS - GET DOCUMENTS 

      db.collection(collection).orderBy("name").get()
        .then(function (querySnapshot) {
          var datacCount = 0;
          querySnapshot.forEach(function (doc) {

            //Increment counter
            datacCount++;

            //Get the object
            var currentDocument = doc.data();

            //Sace uidOfFirebase inside him
            currentDocument.uidOfFirebase = doc.id;

            console.log(doc.id, " => ", currentDocument);

            //Save in the list of documents
            documents.push(currentDocument)
          });
          console.log("DOCS----");
          console.log(documents);

          //Save the douments in the sate
          _this.setState({
            isLoading: false,
            documents: documents,
            showAddCollection: datacCount == 0 ? collection : ""
          })
          if (datacCount == 0) {
            _this.refs.addCollectionDialog.show();
          }
          console.log(_this.state.documents);
        });
    } else {
      //DOCUMENT - GET FIELDS && COLLECTIONS
      var referenceToCollection = collection.replace("/" + lastSegment, "");

      //Create reference to the document itseld
      var docRef = db.collection(referenceToCollection).doc(lastSegment);

      //Get the starting collectoin
      var parrentCollection = segments;
      parrentCollection.splice(-1, 1);

      //Find the collections of this document
      this.findDocumentCollections(parrentCollection);

      docRef.get().then(function (doc) {
        if (doc.exists) {
          console.log("Document data:", doc.data());

          //Directly process the data
          _this.processRecords(doc.data())
        } else {
          console.log("No such document!");
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
      });
    }
  }

  /**
   * Step 3
   * findDocumentCollections - what collections should we display / currently there is no way to get collection form docuemnt
   * @param {Array} chunks - the collection / documents
   */
  findDocumentCollections(chunks) {

    console.log("Search for the schema now of " + chunks);

    //At start is the complete schema
    var theInsertSchemaObject = INSERT_STRUCTURE;
    var cuurrentFields = null;
    console.log("CHUNKS");
    console.log(chunks);

    //Foreach chunks, find the collections / fields
    chunks.map((item, index) => {
      console.log("current chunk:" + item);

      //Also make the last object any
      //In the process, check if we have each element in our schema
      if (theInsertSchemaObject != null && theInsertSchemaObject && theInsertSchemaObject[item] && theInsertSchemaObject[item]['collections']) {
        var isLastObject = (index == (chunks.length - 1));

        if (isLastObject && theInsertSchemaObject != null && theInsertSchemaObject[item] && theInsertSchemaObject[item]['fields']) {
          cuurrentFields = theInsertSchemaObject[item]['fields'];
        }

        if (isLastObject && theInsertSchemaObject != null) {
          //It is last
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        } else {
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        }
      } else {
        theInsertSchemaObject = [];
      }
      console.log("Current schema");
      console.log(theInsertSchemaObject);


    })

    //Save the collection to be shown as button and fieldsOfOnsert that will be used on inserting object
    this.setState({ collections: theInsertSchemaObject, fieldsOfOnsert: cuurrentFields })
  }

  /**
   * Step 4
   * Processes received records from firebase
   * @param {Object} records
   */
  processRecords(records) {
    console.log(records);

    var fields = {};
    var arrays = {};
    var elements = [];
    var elementsInArray = [];
    var newState = {};
    var directValue = "";
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.elements = elements;
    newState.directValue = directValue;
    newState.elementsInArray = elementsInArray;
    newState.records = null;

    this.setState(newState);

    //Each display is consisted of
    //Fields   - This are string, numbers, photos, dates etc...
    //Arrays   - Arrays of data, ex items:[0:{},1:{},2:{}...]
    //         - Or object with prefixes that match in array
    //Elements - Object that don't match in any prefix for Join - They are represented as buttons.

    //In FireStore
    //GeoPoint
    //DocumentReference

    //If record is of type array , then there is no need for parsing, just directly add the record in the arrays list

    console.log(Common.getClass(records));
    if (Common.getClass(records) == "Array") {
      //Get the last name
      console.log("This is array");
      var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
      var allPathItems = subPath.split("+");
      console.log(allPathItems)
      if (allPathItems.length > 0) {
        var lastItem = allPathItems[allPathItems.length - 1];
        console.log(lastItem);
        arrays[lastItem] = records;

      }
      //this.setState({"arrays":this.state.arrays.push(records)})
    } else if (Common.getClass(records) == "Object") {
      //Parse the Object record
      for (var key in records) {
        if (records.hasOwnProperty(key)) {
          var currentElementClasss = Common.getClass(records[key]);
          console.log(key + "'s class is: " + currentElementClasss);

          //Add the items by their type
          if (currentElementClasss == "Array") {
            //Add it in the arrays  list
            arrays[key] = records[key];
          } else if (currentElementClasss == "Object") {
            //Add it in the elements list
            var isElementMentForTheArray = false; //Do we have to put this object in the array
            for (var i = 0; i < Config.adminConfig.prefixForJoin.length; i++) {
              if (key.indexOf(Config.adminConfig.prefixForJoin[i]) > -1) {
                isElementMentForTheArray = true;
                break;
              }
            }

            var objToInsert = records[key];
            //alert(key);
            objToInsert.uidOfFirebase = key;

            if (isElementMentForTheArray) {
              //Add this to the merged elements
              elementsInArray.push(objToInsert);
            } else {
              //Add just to elements
              elements.push(objToInsert);
            }

          } else if (currentElementClasss != "undefined" && currentElementClasss != "null") {
            //This is string, number, or Boolean
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "GeoPoint") {
            //This is GeoPOint
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "DocumentReference") {
            //This is DocumentReference
            //Add it to the fields list
            fields[key] = records[key];
          }

        }
      }
    } if (Common.getClass(records) == "String") {
      console.log("We have direct value of string");
      directValue = records;
    }

    //Convert fields from object to array
    var fieldsAsArray = [];
    console.log("Add the items now inside fieldsAsArray");
    console.log("Current schema");
    console.log(this.state.currentInsertStructure)
    //currentInsertStructure
    var keysFromFirebase = Object.keys(fields);
    console.log("keysFromFirebase")
    console.log(keysFromFirebase)
    var keysFromSchema = Object.keys(this.state.currentInsertStructure || {});
    console.log("keysFromSchema")
    console.log(keysFromSchema)

    //pp_add
    var navigation = Config.navigation;
    var itemFound = false;
    var showFields = null;
    for (var i = 0; i < navigation.length && !itemFound; i++) {
      if (navigation[i].editFields && navigation[i].link == "CoachAdmin") {
        showFields = navigation[i].editFields;
        itemFound = true;
      }
    }
    //end pp_add

    keysFromSchema.forEach((key) => {
      if (fields.hasOwnProperty(key)) {
        fieldsAsArray.push({ "theKey": key, "value": fields[key] })
        var indexOfElementInFirebaseObject = keysFromFirebase.indexOf(key);
        if (indexOfElementInFirebaseObject > -1) {
          keysFromFirebase.splice(indexOfElementInFirebaseObject, 1);
        }
      }
    });

    console.log("keysFromFirebase")
    console.log(keysFromFirebase)

    var pos = -1;

    keysFromFirebase.forEach((key) => {
      if (fields.hasOwnProperty(key)) {
        if (showFields)
          pos = showFields.indexOf(key);
        if (pos < 0)
          return;
        // fieldsAsArray.push({ "theKey": key, "value": fields[key] })
        // fieldsAsArray.splice(pos, 0, { "theKey": key, "value": fields[key] })
        fieldsAsArray[pos] =  { "theKey": key, "value": fields[key] }
      }
    });



    //Get all array names
    var arrayNames = [];
    Object.keys(arrays).forEach((key) => {
      arrayNames.push(key)
    });

    var newState = {};
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.isJustArray = Common.getClass(records) == "Array";
    newState.elements = elements;
    newState.elementsInArray = elementsInArray;
    newState.directValue = directValue;
    newState.records = records;
    newState.isLoading = false;

    console.log("THE elements")
    console.log(elements);

    //Set the new state
    this.setState(newState);

    //Additional init, set the DataTime, check format if something goes wrong
    window.additionalInit();
  }

  /**
   *
   * Create R Update D
   *
   */

  /**
  * processValueToSave  - helper for saving in Firestore , converts value to correct format
  * @param {value} value
  * @param {type} type of field
  */
  processValueToSave(value, type) {
    //To handle number values
    if (!isNaN(value)) {
      value = Number(value);
    }

    //To handle boolean values
    value = value === "true" ? true : (value === "false" ? false : value);


    if (type == "date") {
      //To handle date values
      if (moment(value).isValid()) {
        value = moment(value).toDate();
        //futureStartAtDate = new Date(moment().locale("en").add(1, 'd').format("MMM DD, YYYY HH:MM"))
      }
    }

    return value;
  }

  /**
  * updatePartOfObject  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {String} firebasePath current firestore path
  * @param {String} byGivvenSubLink force link to field
  * @param {Function} callback function after action
  */
  updatePartOfObject(key, value, dorefresh = false, type = null, firebasePath, byGivvenSubLink = null, callback = null) {
    var subLink = this.state.theSubLink;
    if (byGivvenSubLink != null) {
      subLink = byGivvenSubLink;
    }
    console.log("Sub save " + key + " to " + value + " and the path is " + firebasePath + " and theSubLink is " + subLink);
    var chunks = subLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
    var _this = this;
    //First get the document
    //DOCUMENT - GET FIELDS && COLLECTIONS
    var docRef = firebase.app.firestore().doc(firebasePath);
    docRef.get().then(function (doc) {
      if (doc.exists) {
        var numChunks = chunks.length - 1;
        var doc = doc.data();
        if (value == "DELETE_VALUE") {
          if (numChunks == 2) {
            doc[chunks[1]].splice(chunks[2], 1);
          }
          if (numChunks == 1) {
            doc[chunks[1]] = null;
          }
        } else {
          //Normal update, or insert
          if (numChunks == 2) {
            doc[chunks[1]][chunks[2]] = value
          }
          if (numChunks == 1) {
            doc[chunks[1]][key] = value
          }
        }

        console.log("Document data:", doc);
        _this.updateAction(chunks[1], doc[chunks[1]], dorefresh, null, true)
        if (callback) {
          callback();
        }

        //alert(chunks.length-1);
        //_this.processRecords(doc.data())
        //console.log(doc);

      } else {
        console.log("No such document!");
      }
    }).catch(function (error) {
      console.log("Error getting document:", error);
    });

  }

  //to save fields edited in edit page
  saveEdits() {
    var firebasePath = (this.props.route.path.replace("/CoachAdmin/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    var db = firebase.app.firestore();

    //var databaseRef = db.doc(firebasePath);
    var databaseRef = db.doc(this.state.firebasePath);
    var updateObj = {};
    var i;
    for (i = this.state.editedFields.length - 1; i >= 0; --i) {
      updateObj[this.state.editedFields[i]] = this.state[this.state.editedFields[i]];
      this.state.editedFields.splice(i, 1);
    }
    databaseRef.set(updateObj, { merge: true });

  }

  resetEdits() {
    this.state.editedFields.splice(0, this.state.editedFields.length);
  }

  /**
  * Firebase update based on key / value,
  * This function also sets derect name and value
  * @param {String} key
  * @param {String} value
  */

  /**
  * updateAction  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {Boolean} forceObjectSave force saving sub object
  */
  updateAction(key, value, dorefresh = true, type = null, forceObjectSave = false) {
    value = this.processValueToSave(value, type);
    var firebasePath = (this.props.route.path.replace("/CoachAdmin/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (this.state.theSubLink != null && !forceObjectSave) {
      this.updatePartOfObject(key, value, dorefresh, type, firebasePath)
    } else {

      //value=firebase.firestore().doc("/users/A2sWwzDop0EAMdfxfJ56");
      //key="creator";

      console.log("firebasePath from update:" + firebasePath)
      console.log('Update ' + key + " into " + value);

      if (key == "NAME_OF_THE_NEW_KEY" || key == "VALUE_OF_THE_NEW_KEY") {
        console.log("THE_NEW_KEY")
        var updateObj = {};
        updateObj[key] = value;
        this.setState(updateObj);
        console.log(updateObj);
      } else {
        //PP: DONT UPDATE THE DB HERE. WAIT FOR THE SAVE
        if (!this.state.editedFields.includes(key))
          this.state.editedFields.push(key);

        if (key == "photo") {
          this.state.imageLoading = false;
        }

        if (key == "date_of_birth") {

          this.setState({ swimGroup: this.getGroup(value) });
        }

        this.setState({ [key]: value });
      }

    }
  }

  /**
  * addDocumentToCollection  - used recursivly to add collection's document's collections
  * @param {String} name name of the collection
  * @param {FirestoreReference} reference
  */
  addDocumentToCollection(name, reference = null) {

    var pathChunks = this.state.firebasePath.split("/");
    pathChunks.pop();
    var withoutLast = pathChunks.join("/");
    console.log(name + " vs " + withoutLast);
    //Find the fields to be inserted
    var theInsertSchemaObject = INSERT_STRUCTURE[name].fields;
    console.log(JSON.stringify(theInsertSchemaObject));

    //Find the collections to be inserted
    var theInsertSchemaCollections = INSERT_STRUCTURE[name].collections;
    console.log(JSON.stringify(theInsertSchemaCollections));

    //Reference to root firestore or existing document reference
    var db = reference == null ? (pathChunks.length > 1 ? firebase.app.firestore().doc(withoutLast) : firebase.app.firestore()) : reference;

    //Check type of insert
    var isTimestamp = Config.adminConfig.methodOfInsertingNewObjects == "timestamp"

    //Create new element
    var newElementRef = isTimestamp ? db.collection(name).doc(Date.now()) : db.collection(name).doc()

    //Add data to the new element
    //newElementRef.set(theInsertSchemaObject)

    //Go over sub collection and insert them
    for (var i = 0; i < theInsertSchemaCollections.length; i++) {
      this.addDocumentToCollection(theInsertSchemaCollections[i], newElementRef)
    }


    //Show the notification on root element
    if (reference == null) {
      this.cancelAddFirstItem();
      this.setState({ notifications: [{ type: "success", content: "Element added. You can find it in the table bellow." }] });
      this.refreshDataAndHideNotification();
    }
  }

  /**
  * addKey
  * Adds key in our list of fields in firestore
  */
  addKey() {
    if (this.state.NAME_OF_THE_NEW_KEY && this.state.NAME_OF_THE_NEW_KEY.length > 0) {

      if (this.state.VALUE_OF_THE_NEW_KEY && this.state.VALUE_OF_THE_NEW_KEY.length > 0) {

        this.setState({ notifications: [{ type: "success", content: "New key added." }] });
        this.updateAction(this.state.NAME_OF_THE_NEW_KEY, this.state.VALUE_OF_THE_NEW_KEY);
        this.refs.simpleDialog.hide();
        this.refreshDataAndHideNotification();
      }
    }
  }

  /**
  * addItemToArray  - add item to array
  * @param {String} name name of the array
  * @param {Number} howLongItIs count of items, to know the next index
  */
  addItemToArray(name, howLongItIs) {
    console.log("Add item to array " + name);
    console.log("Is just array " + this.state.isJustArray);

    console.log("Data ");
    console.log(this.state.fieldsOfOnsert);

    var dataToInsert = null;
    var correctPathToInsertIn = "";
    if (this.state.fieldsOfOnsert) {
      if (this.state.isJustArray) {
        console.log("THIS IS Array")
        dataToInsert = this.state.fieldsOfOnsert[0];
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      } else {
        dataToInsert = this.state.fieldsOfOnsert[name];
        dataToInsert = dataToInsert ? dataToInsert[0] : null;
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + name + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      }
    }

    console.log("Data to insert");
    console.log(dataToInsert);
    console.log("Path to insert");
    console.log(correctPathToInsertIn);

    var _this = this;
    this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", dataToInsert, true, null, this.state.firebasePath, correctPathToInsertIn, function (e) {
      _this.setState({ notifications: [{ type: "success", content: "New element added." }] });
      _this.refreshDataAndHideNotification();
    })
  }

  /**
  *
  * C Read U D
  *
  */

  /**
  * showSubItems - displays sub object, mimics opening of new page
  * @param {String} theSubLink , direct link to the sub object
  */
  showSubItems(theSubLink) {
    var chunks = theSubLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
    this.setState({
      itemOfInterest: chunks[1],
      theSubLink: theSubLink,
    })
    var items = this.state.records;
    for (var i = 1; i < chunks.length; i++) {
      console.log(chunks[i]);
      items = items[chunks[i]];
    }
    console.log("--- NEW ITEMS ");
    console.log(items)
    this.processRecords(items);
  }

  /**
  *
  * C R U Delete
  *
  */

  /**
  * deleteFieldAction - displays sub object, mimics opening of new page
  * @param {String} key to be updated
  * @param {Boolean} isItArrayItem 
  * @param {String} theLink 
  */
    deleteFieldAction(){
      this.showAlert("deleteAthleteAlert");
    }
    //  Commented because delete action is not required
  // deleteFieldAction(key, isItArrayItem = false, theLink = null) {
  //   console.log("Delete " + key);
  //   console.log(theLink);
  //   if (theLink != null) {
  //     theLink = theLink.replace("/CoachAdmin", "");
  //   }
  //   if (isNaN(key)) {
  //     isItArrayItem = false;
  //   }
  //   console.log("Is it array: " + isItArrayItem);
  //   var firebasePathToDelete = (this.props.route.path.replace(ROUTER_PATH, "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
  //   if (key != null) {
  //     //firebasePathToDelete+=("/"+key)
  //   }

  //   console.log("firebasePath for delete:" + firebasePathToDelete);
  //   this.setState({ pathToDelete: theLink ? theLink : firebasePathToDelete, isItArrayItemToDelete: isItArrayItem, keyToDelete: theLink ? "" : key });

  //   this.showAlert("deleteAlert");

  // }

  /**
  * doDelete - do the actual deleting based on the data in the state
  */
  doDelete() {
    var _this = this;

    var completeDeletePath = this.state.pathToDelete + "/" + this.state.keyToDelete;

    if (this.state.pathToDelete.indexOf(Config.adminConfig.urlSeparatorFirestoreSubArray) > -1) {
      //Sub data
      _this.refs.deleteDialog.hide();
      this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", "DELETE_VALUE", true, null, this.state.firebasePath, this.state.pathToDelete, function (e) {
        _this.setState({ notifications: [{ type: "success", content: "Element deleted." }] });
        _this.refreshDataAndHideNotification();
      })
    } else {
      //Normal data
      var chunks = completeDeletePath.split("/");

      var db = firebase.app.firestore();


      if (chunks.length % 2) {
        //odd
        //Delete fields from docuemnt
        var refToDoc = db.doc(this.state.pathToDelete);

        // Remove the 'capital' field from the document
        var deleteAction = {};
        deleteAction[this.state.keyToDelete] = firebaseREF.firestore.FieldValue.delete();
        var removeKey = refToDoc.update(deleteAction).then(function () {
          console.log("Document successfully deleted!");

          _this.setState({ deleteAlert: false, keyToDelete: null, pathToDelete: null, notifications: [{ type: "success", content: "Field is deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing document: ", error);
        });
      } else {
        //even
        //delete document from collection
        //alert("Delete document "+completeDeletePath);
        //db.collection(this.state.pathToDelete).doc(this.state.keyToDelete).delete().then(function() {
        db.collection(this.state.firebasePath).doc(this.state.keyToDelete).delete().then(function () {
          console.log("Document successfully deleted!");

          _this.setState({ deleteAlert: false, pathToDelete: null, notifications: [{ type: "success", content: "Coach deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing document: ", error);
        });

      }

    }




    /*firebase.database().ref(this.state.pathToDelete).set(null).then((e)=>{
      console.log("Delete res: "+e)
      this.refs.deleteDialog.hide();
      this.setState({keyToDelete:null,pathToDelete:null,notifications:[{type:"success",content:"Field is deleted."}]});
      this.refreshDataAndHideNotification();

    })*/
  }

  /**
  * cancelDelete - user click on cancel
  */
  cancelDelete() {
    console.log("Cancel Delete");
    this.refs.deleteDialog.hide()
  }

  cancelAddFirstItem() {
    console.log("Cancel Add");
    this.refs.addCollectionDialog.hide()
  }



  /**
  *
  * UI GENERATORS
  *
  */

  /**
  * This function finds the headers for the current menu
  * @param firebasePath - we will use current firebasePath to find the current menu
  */
  findHeadersBasedOnPath(firebasePath) {
    var headers = null;

    var itemFound = false;
    var navigation = Config.navigation;
    for (var i = 0; i < navigation.length && !itemFound; i++) {
      if (navigation[i].path == "coaches" && navigation[i].tableFields && navigation[i].link == "CoachAdmin") {
        headers = navigation[i].tableFields;
        itemFound = true;
      }

      //Look into the sub menus
      if (navigation[i].subMenus) {
        for (var j = 0; j < navigation[i].subMenus.length; j++) {
          if (navigation[i].subMenus[j].path == firebasePath && navigation[i].subMenus[j].tableFields && navigation[i].subMenus[j].link == "CoachAdmin") {
            headers = navigation[i].subMenus[j].tableFields;
            itemFound = true;
          }
        }
      }
    }
    return headers;
  }

  /**
  * makeCollectionTable
  * Creates single collection documents
  */
  makeCollectionTable() {
    var name = this.state.currentCollectionName;
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          {/*begin pp_add*/}
          {/*
                  <a  onClick={()=>{this.addDocumentToCollection(name)}}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{float:"right"}}> */}
          <a style={{ cursor: "pointer" }} onClick={() => { this.refs.addnewitemDialog.show() }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
            {/*end pp_add*/}

            <i className="material-icons">add</i>
          </div></a>
          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                deleteFieldAction={this.deleteFieldAction}
                fromObjectInArray={true}
                name={name}
                routerPath={this.props.route.path}
                isJustArray={false}
                sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                data={this.state.documents}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
   * Creates single array section
   * @param {String} name, used as key also
   */
  makeArrayCard(name) {
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          <a onClick={() => { this.addItemToArray(name, this.state.arrays[name].length) }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
            <i className="material-icons">add</i>
          </div></a>
          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                isFirestoreSubArray={true}
                showSubItems={this.showSubItems}
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                deleteFieldAction={this.deleteFieldAction}
                fromObjectInArray={false} name={name}
                routerPath={this.props.route.path}
                isJustArray={this.state.isJustArray}
                sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                data={this.state.arrays[name]} />
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
   * Creates  table section for the elements object
   * @param {String} name, used as key also
   */
  makeTableCardForElementsInArray() {
    var name = this.state.lastPathItem;
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                isFirestoreSubArray={true}
                showSubItems={this.showSubItems}
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)} deleteFieldAction={this.deleteFieldAction} fromObjectInArray={true} name={name} routerPath={this.props.route.path} isJustArray={this.state.isJustArray} sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""} data={this.state.elementsInArray}>
              </Table>
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
    * Creates direct value section
    * @param {String} value, valu of the current path
    */
  makeValueCard(value) {
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          <div className="card-content">
            <h4 className="card-title">Value</h4>
            <div className="toolbar">
            </div>
            <div>
              <Input updateAction={this.updateAction} className="" theKey="DIRECT_VALUE_OF_CURRENT_PATH" value={value} />
            </div>
          </div>
        </div>
      </div>
    )
  }


  /**
   * generateBreadCrumb
   */
  generateBreadCrumb() {
    var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
    var items = subPath.split(Config.adminConfig.urlSeparator);
    var path = "/CoachAdmin/";
    return (<div>{items.map((item, index) => {
      if (index == 0) {
        path += item;
      } else {
        path += "+" + item;
      }

      return (<Link className="navbar-brand" to={path}>{item} <span className="breadcrumbSeparator">{index == items.length - 1 ? "" : "/"}</span><div className="ripple-container"></div></Link>)
    })}</div>)
  }

  /**
   * generateNotifications
   * @param {Object} item - notification to be created
   */
  generateNotifications(item) {
    return (
      <div className="col-md-12">
        <Notification type={item.type} >{item.content}</Notification>
      </div>
    )
  }

  /**
  * refreshDataAndHideNotification
  * @param {Boolean} refreshData 
  * @param {Number} time 
  */
  refreshDataAndHideNotification(refreshData = true, time = 3000) {
    //Refresh data,
    if (refreshData) {
      this.resetDataFunction();
    }

    //Hide notifications
    setTimeout(function () { this.setState({ notifications: [] }) }.bind(this), time);
  }


  formValueCapture(k, v) {
    //alert(k);
    this.setState({ [k]: v });
  }

  cancelAddNewItem() {
    var newState = {};
    newState.full_name = "";
    newState.gender = "",
      newState.date_of_birth = this.startDOB();
    newState.blood_group = "O+";
    newState.address = "";
    newState.city = "Bangalore",
      newState.pin_code = "",
      newState.country = "India",
      newState.email = "";
    newState.mobile_number = "";
    newState.photo = "https://firebasestorage.googleapis.com/v0/b/kreed-of-sports.appspot.com/o/photos%2Fdefault_profile_350x400.png?alt=media&token=0741bfbb-8f54-478e-9339-32e520d66c92";
    newState.editedFields = [];
    newState.imageLoading = false;

    this.setState(newState);

    this.refs.addnewitemDialog.hide();
  }
  //Validate inputs
  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    if (name == "mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }

    if (name == "pin_code") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 6) {
        return false;
      }
    }

    this.setState({
      [name]: value
    });
  }

  validateInputs() {
    if (this.state.mobile_number.length < 10 || this.state.pin_code.length < 6 || this.state.gender === "") {
      return false;
    }
    return true;
  }
  validatePhoto() {
    if (this.state.photo == "https://firebasestorage.googleapis.com/v0/b/kreed-of-sports.appspot.com/o/photos%2Fdefault_profile_350x400.png?alt=media&token=0741bfbb-8f54-478e-9339-32e520d66c92") {
      return false;
    }
    return true;

  }
  markUploaderStart() {
    this.state.imageLoading = true;
  }

  //addition of new coach and pushing to database
  addNewItem(e) {
    e.preventDefault(); // <- prevent form submit from reloading the page
    /* Send the message to Firebase */
    if (this.state.imageLoading) {
      this.showAlert("loadingAlert");
      return;
    }
    var db = firebase.app.firestore();

    if (!this.validatePhoto()) {
      this.showAlert("photoAlert");
      return false;
    }
    // this.inputData();
    if (!this.validateInputs()) {
      this.showAlert("phoneAlert");
      return false;
    }
    var collection = this.state.firebasePath;

    if (!this.state.debounce) {
      this.state.debounce = true;

      db.collection(collection).add({
        name: this.state.full_name,
        gender: this.state.gender,
        date_of_birth: this.state.date_of_birth,
        blood_group: this.state.blood_group,
        address: this.state.address,
        city: this.state.city,
        pin_code: this.state.pin_code,
        country: this.state.country,
        email: this.state.email,
        mobile_number: this.state.mobile_number,
        photo: this.state.photo
      })
        .then(docRef => {
          //cleanup and reset state
          this.refs.addCollectionDialog.hide()
          this.refs.addnewitemDialog.hide()
          this.refreshDataAndHideNotification()
          this.setState({ debounce: false, notifications: [{ type: "success", content: "New Coach Added." }] })
        })
        .catch(function (error) {
          alert("Error adding document: " + error);
        });
    }
  }

  //MAIN RENDER FUNCTION 
  render() {
    // Styling for add new caoch skylight
    var addnewitemDialog = {
      width: '70%',
      height: 'auto',
      marginLeft: '-35%',
      position: 'absolute',
      padding: '0px'
    };
    // Styling for add Photo guidelines skylight
    var photoGuidelines = {
      height: 'auto',
      position: 'absolute',
      padding: '0px'
    }
    return (
      <div className="content">
        <NavBar>
          <div className="pull-right">
            <a href="http://karnatakaswimming.org" target="_blank"> <img className="img-circle" src='assets/img/ksa_logo.png' height="70" width="80" /></a>
          </div>
        </NavBar>


        <div className="content" sub={this.state.lastSub}>

          <div className="container-fluid">

            <div style={{ textAlign: 'center' }}>
              {/* LOADER */}
              {this.state.isLoading ? <Loader color="#8637AD" size="12px" margin="4px" /> : ""}
            </div>

            {/* NOTIFICATIONS */}
            {this.state.notifications ? this.state.notifications.map((notification) => {
              return this.generateNotifications(notification)
            }) : ""}

            {/* sweet photo alert */}
            <SweetAlert warning
              show={this.state.photoAlert}
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ photoAlert: false })}>
              You must upload an ID card photo by clicking on the photo control and then hit the green checkmark to apply
              </SweetAlert>

              <SweetAlert warning
              show={this.state.deleteAthleteAlert}
              confirmBtnBsStyle="warning"
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ deleteAthleteAlert: false })}>
              You can Edit the Coach details at the EDIT window.<br/>
              If you still want to delete an Coach,<br/>
              please write an email to contact@swimindia.in or call us 9108456704
              </SweetAlert>

            {/* sweet phone alert */}
            <SweetAlert
              warning
              show={this.state.phoneAlert}
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ phoneAlert: false })}>
              Please ensure you have selected a gender and have entered a 10 digit mobile number and a 6 digit PIN code!
              </SweetAlert>

            {/* sweet photo loading alert */}
            <SweetAlert
              show={this.state.loadingAlert}
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ loadingAlert: false })}>
              Please wait! Files/Images are uploading...
              </SweetAlert>

            {/* sweet delete alert */}
            <SweetAlert
              warning
              showCancel
              show={this.state.deleteAlert}
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="deleteAlertBtnColor"
              cancelBtnBsStyle="default"
              title="Are you sure?"
              onConfirm={() => this.doDelete()}
              onCancel={() => this.setState({ deleteAlert: false })}
            >
              All data associated with this coach will be deleted!
              </SweetAlert>


            {/* Documents in collection */}
            {this.state.isCollection && this.state.documents.length > 0 ? this.makeCollectionTable() : ""}

            {/* DIRECT VALUE */}
            {this.state.directValue && this.state.directValue.length > 0 ? this.makeValueCard(this.state.directValue) : ""}


            {/* FIELDS */}
            {this.state.fieldsAsArray && this.state.fieldsAsArray.length > 0 ? (<div className="col-md-12">
              <div className="card">
                {/*
                <a  onClick={()=>{this.refs.simpleDialog.show()}}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{float:"right"}}>
                    <i className="material-icons">add</i>
                </div></a>*/}
                <form className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{Common.capitalizeFirstLetter(Config.adminConfig.fieldBoxName)}</h4>
                  </div>
                  {this.state.fieldsAsArray ? this.state.fieldsAsArray.map((item) => {

                    return (
                      <Fields
                        isFirestore={true}
                        parentKey={null}
                        key={item.theKey + this.state.lastSub}
                        deleteFieldAction={this.deleteFieldAction}
                        updateAction={this.updateAction}
                        theKey={item.theKey}
                        value={item.value} />)


                  }) : ""}
                  <div
                    className="text-center">

                    <Link to='/CoachAdmin/coaches'>
                      <button onClick={this.saveEdits}
                        className="btn btn-rose btn-wd margin-bottom-10px btn-width"
                        value="Save">Save</button>
                    </Link>

                    <Link to='/CoachAdmin/coaches'>
                      <button
                        className="btn btn-rose btn-wd margin-bottom-10px btn-width"
                        value="Cancel">Cancel</button>
                    </Link>

                  </div>

                </form>
              </div>
            </div>) : ""}


            {/* COLLECTIONS */}
            {this.state.theSubLink == null && this.state.isDocument && this.state.collections && this.state.collections.length > 0 ? (<div className="col-md-12">
              <div className="card">

                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{"Collections"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.theSubLink == null && this.state.collections ? this.state.collections.map((item) => {
                      var theLink = "/CoachAdmin/" + this.state.completePath + Config.adminConfig.urlSeparator + item;
                      return (<Link to={theLink}><a className="btn">{item}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>


                </form>
              </div>
            </div>) : ""}

            {/* ARRAYS */}
            {this.state.arrayNames ? this.state.arrayNames.map((key) => {
              return this.makeArrayCard(key)
            }) : ""}

            {/* ELEMENTS MERGED IN ARRAY */}
            {this.state.elementsInArray && this.state.elementsInArray.length > 0 ? (this.makeTableCardForElementsInArray()) : ""}

            {/* ELEMENTS */}
            {this.state.elements && this.state.elements.length > 0 ? (<div className="col-md-12">
              <div className="card">

                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{this.state.lastPathItem + "' elements"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.elements ? this.state.elements.map((item) => {
                      var theLink = "/fireadmin/" + this.state.completePath + Config.adminConfig.urlSeparatorFirestoreSubArray + item.uidOfFirebase;
                      return (<Link onClick={() => { this.showSubItems(theLink) }}><a className="btn">{item.uidOfFirebase}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>


                </form>
              </div>
            </div>) : ""}




          </div>
        </div>
        {/* Skylight for adding first coach */}
        <SkyLight hideOnOverlayClicked ref="addCollectionDialog" title="">
          <span><h3 className="center-block">Add the first coach in your club</h3></span>
          <div className="col-md-12">
            <Notification type="success" >Looks like there are no coaches yet. Add your first coach.</Notification>
          </div>

          {/* <div className="col-md-12">
            Data Location
          </div>
          <div className="col-md-12">
            <b>{this.state.showAddCollection}</b>
          </div> */}


          <div className="col-sm-12" style={{ marginTop: 80 }}>
            <div className="col-sm-6">
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.cancelAddFirstItem} className="btn btn-info center-block">Cancel</a>
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={() => { this.refs.addnewitemDialog.show() }} className="btn btn-success center-block">ADD</a>
            </div>

          </div>

        </SkyLight>
        {/* End Skylight for adding first coach */}
        {/* Skylight for adding new coach */}
        {/*begin pp_add*/}
        <SkyLight dialogStyles={addnewitemDialog} ref="addnewitemDialog">
          <div className="card-content ">
            <div className="card margin-tb">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">location_on</i>
              </div>
              <h4>Add New Coach</h4>

              <form className="padding-10" onSubmit={this.addNewItem}>
                <div className="row">
                  <div className="col-sm-6">
                    {/* Full name */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color">Full Name :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="text" required="true" name="full_name" aria-required="true" value={this.state.full_name} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Full name */}
                    {/* gender */}
                    <div className="row margin-t-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color">Gender :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <Radio theKey="gender" updateAction={this.updateAction} value={this.state.gender} options={["male", "female"]} />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* gender */}
                    {/* Date of Birth */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color" >Date Of Birth :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <KreedDatePicker theKey="date_of_birth" value={this.state.date_of_birth} updateAction={this.updateAction} />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Date of Birth */}
                    {/* Blood Groups */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right padding-right-5">
                        <label className="control-label label-color" >Blood Group :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <Select theKey="blood_group" updateAction={this.updateAction} value={this.state.blood_group} options={["O+", "O-", "A+", "A-", "B+", "B-", "AB+", "AB-"]} />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Blood Groups */}
                    {/* address */}
                    <div className="row margin-b-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color" >Home Address :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <textarea required="true" name="address" value={this.state.address} onChange={this.handleInputChange} className="form-control" rows="4" cols="21" />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* address */}
                    {/* city */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color" >City :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="text" required="true" name="city" aria-required="true" value={this.state.city} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* city */}
                    {/* pincode */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color" >Pin Code :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="text" required="true" name="pin_code" aria-required="true" value={this.state.pin_code} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* pincode */}
                    {/* country */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color" >Country :</label>
                      </div>
                      <div className="col-sm-6 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <select className="select_width" name="country" aria-required="true" value={this.state.country} onChange={this.handleInputChange}>
                              <option value="Afghanistan">Afghanistan</option>
                              <option value="Albania">Albania</option>
                              <option value="Algeria">Algeria</option>
                              <option value="American Samoa">American Samoa</option>
                              <option value="Andorra">Andorra</option>
                              <option value="Angola">Angola</option>
                              <option value="Anguilla">Anguilla</option>
                              <option value="Antartica">Antarctica</option>
                              <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                              <option value="Argentina">Argentina</option>
                              <option value="Armenia">Armenia</option>
                              <option value="Aruba">Aruba</option>
                              <option value="Australia">Australia</option>
                              <option value="Austria">Austria</option>
                              <option value="Azerbaijan">Azerbaijan</option>
                              <option value="Bahamas">Bahamas</option>
                              <option value="Bahrain">Bahrain</option>
                              <option value="Bangladesh">Bangladesh</option>
                              <option value="Barbados">Barbados</option>
                              <option value="Belarus">Belarus</option>
                              <option value="Belgium">Belgium</option>
                              <option value="Belize">Belize</option>
                              <option value="Benin">Benin</option>
                              <option value="Bermuda">Bermuda</option>
                              <option value="Bhutan">Bhutan</option>
                              <option value="Bolivia">Bolivia</option>
                              <option value="Bosnia and Herzegowina">Bosnia and Herzegowina</option>
                              <option value="Botswana">Botswana</option>
                              <option value="Bouvet Island">Bouvet Island</option>
                              <option value="Brazil">Brazil</option>
                              <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                              <option value="Brunei Darussalam">Brunei Darussalam</option>
                              <option value="Bulgaria">Bulgaria</option>
                              <option value="Burkina Faso">Burkina Faso</option>
                              <option value="Burundi">Burundi</option>
                              <option value="Cambodia">Cambodia</option>
                              <option value="Cameroon">Cameroon</option>
                              <option value="Canada">Canada</option>
                              <option value="Cape Verde">Cape Verde</option>
                              <option value="Cayman Islands">Cayman Islands</option>
                              <option value="Central African Republic">Central African Republic</option>
                              <option value="Chad">Chad</option>
                              <option value="Chile">Chile</option>
                              <option value="China">China</option>
                              <option value="Christmas Island">Christmas Island</option>
                              <option value="Cocos Islands">Cocos (Keeling) Islands</option>
                              <option value="Colombia">Colombia</option>
                              <option value="Comoros">Comoros</option>
                              <option value="Congo">Congo</option>
                              <option value="Congo">Congo, the Democratic Republic of the</option>
                              <option value="Cook Islands">Cook Islands</option>
                              <option value="Costa Rica">Costa Rica</option>
                              <option value="Cota D'Ivoire">Cote d'Ivoire</option>
                              <option value="Croatia">Croatia (Hrvatska)</option>
                              <option value="Cuba">Cuba</option>
                              <option value="Cyprus">Cyprus</option>
                              <option value="Czech Republic">Czech Republic</option>
                              <option value="Denmark">Denmark</option>
                              <option value="Djibouti">Djibouti</option>
                              <option value="Dominica">Dominica</option>
                              <option value="Dominican Republic">Dominican Republic</option>
                              <option value="East Timor">East Timor</option>
                              <option value="Ecuador">Ecuador</option>
                              <option value="Egypt">Egypt</option>
                              <option value="El Salvador">El Salvador</option>
                              <option value="Equatorial Guinea">Equatorial Guinea</option>
                              <option value="Eritrea">Eritrea</option>
                              <option value="Estonia">Estonia</option>
                              <option value="Ethiopia">Ethiopia</option>
                              <option value="Falkland Islands">Falkland Islands (Malvinas)</option>
                              <option value="Faroe Islands">Faroe Islands</option>
                              <option value="Fiji">Fiji</option>
                              <option value="Finland">Finland</option>
                              <option value="France">France</option>
                              <option value="France Metropolitan">France, Metropolitan</option>
                              <option value="French Guiana">French Guiana</option>
                              <option value="French Polynesia">French Polynesia</option>
                              <option value="French Southern Territories">French Southern Territories</option>
                              <option value="Gabon">Gabon</option>
                              <option value="Gambia">Gambia</option>
                              <option value="Georgia">Georgia</option>
                              <option value="Germany">Germany</option>
                              <option value="Ghana">Ghana</option>
                              <option value="Gibraltar">Gibraltar</option>
                              <option value="Greece">Greece</option>
                              <option value="Greenland">Greenland</option>
                              <option value="Grenada">Grenada</option>
                              <option value="Guadeloupe">Guadeloupe</option>
                              <option value="Guam">Guam</option>
                              <option value="Guatemala">Guatemala</option>
                              <option value="Guinea">Guinea</option>
                              <option value="Guinea-Bissau">Guinea-Bissau</option>
                              <option value="Guyana">Guyana</option>
                              <option value="Haiti">Haiti</option>
                              <option value="Heard and McDonald Islands">Heard and Mc Donald Islands</option>
                              <option value="Holy See">Holy See (Vatican City State)</option>
                              <option value="Honduras">Honduras</option>
                              <option value="Hong Kong">Hong Kong</option>
                              <option value="Hungary">Hungary</option>
                              <option value="Iceland">Iceland</option>
                              <option value="India">India</option>
                              <option value="Indonesia">Indonesia</option>
                              <option value="Iran">Iran (Islamic Republic of)</option>
                              <option value="Iraq">Iraq</option>
                              <option value="Ireland">Ireland</option>
                              <option value="Israel">Israel</option>
                              <option value="Italy">Italy</option>
                              <option value="Jamaica">Jamaica</option>
                              <option value="Japan">Japan</option>
                              <option value="Jordan">Jordan</option>
                              <option value="Kazakhstan">Kazakhstan</option>
                              <option value="Kenya">Kenya</option>
                              <option value="Kiribati">Kiribati</option>
                              <option value="Democratic People's Republic of Korea">Korea, Democratic People's Republic of</option>
                              <option value="Korea">Korea, Republic of</option>
                              <option value="Kuwait">Kuwait</option>
                              <option value="Kyrgyzstan">Kyrgyzstan</option>
                              <option value="Lao">Lao People's Democratic Republic</option>
                              <option value="Latvia">Latvia</option>
                              <option value="Lebanon">Lebanon</option>
                              <option value="Lesotho">Lesotho</option>
                              <option value="Liberia">Liberia</option>
                              <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                              <option value="Liechtenstein">Liechtenstein</option>
                              <option value="Lithuania">Lithuania</option>
                              <option value="Luxembourg">Luxembourg</option>
                              <option value="Macau">Macau</option>
                              <option value="Macedonia">Macedonia, The Former Yugoslav Republic of</option>
                              <option value="Madagascar">Madagascar</option>
                              <option value="Malawi">Malawi</option>
                              <option value="Malaysia">Malaysia</option>
                              <option value="Maldives">Maldives</option>
                              <option value="Mali">Mali</option>
                              <option value="Malta">Malta</option>
                              <option value="Marshall Islands">Marshall Islands</option>
                              <option value="Martinique">Martinique</option>
                              <option value="Mauritania">Mauritania</option>
                              <option value="Mauritius">Mauritius</option>
                              <option value="Mayotte">Mayotte</option>
                              <option value="Mexico">Mexico</option>
                              <option value="Micronesia">Micronesia, Federated States of</option>
                              <option value="Moldova">Moldova, Republic of</option>
                              <option value="Monaco">Monaco</option>
                              <option value="Mongolia">Mongolia</option>
                              <option value="Montserrat">Montserrat</option>
                              <option value="Morocco">Morocco</option>
                              <option value="Mozambique">Mozambique</option>
                              <option value="Myanmar">Myanmar</option>
                              <option value="Namibia">Namibia</option>
                              <option value="Nauru">Nauru</option>
                              <option value="Nepal">Nepal</option>
                              <option value="Netherlands">Netherlands</option>
                              <option value="Netherlands Antilles">Netherlands Antilles</option>
                              <option value="New Caledonia">New Caledonia</option>
                              <option value="New Zealand">New Zealand</option>
                              <option value="Nicaragua">Nicaragua</option>
                              <option value="Niger">Niger</option>
                              <option value="Nigeria">Nigeria</option>
                              <option value="Niue">Niue</option>
                              <option value="Norfolk Island">Norfolk Island</option>
                              <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                              <option value="Norway">Norway</option>
                              <option value="Oman">Oman</option>
                              <option value="Pakistan">Pakistan</option>
                              <option value="Palau">Palau</option>
                              <option value="Panama">Panama</option>
                              <option value="Papua New Guinea">Papua New Guinea</option>
                              <option value="Paraguay">Paraguay</option>
                              <option value="Peru">Peru</option>
                              <option value="Philippines">Philippines</option>
                              <option value="Pitcairn">Pitcairn</option>
                              <option value="Poland">Poland</option>
                              <option value="Portugal">Portugal</option>
                              <option value="Puerto Rico">Puerto Rico</option>
                              <option value="Qatar">Qatar</option>
                              <option value="Reunion">Reunion</option>
                              <option value="Romania">Romania</option>
                              <option value="Russia">Russian Federation</option>
                              <option value="Rwanda">Rwanda</option>
                              <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                              <option value="Saint LUCIA">Saint LUCIA</option>
                              <option value="Saint Vincent">Saint Vincent and the Grenadines</option>
                              <option value="Samoa">Samoa</option>
                              <option value="San Marino">San Marino</option>
                              <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                              <option value="Saudi Arabia">Saudi Arabia</option>
                              <option value="Senegal">Senegal</option>
                              <option value="Seychelles">Seychelles</option>
                              <option value="Sierra">Sierra Leone</option>
                              <option value="Singapore">Singapore</option>
                              <option value="Slovakia">Slovakia (Slovak Republic)</option>
                              <option value="Slovenia">Slovenia</option>
                              <option value="Solomon Islands">Solomon Islands</option>
                              <option value="Somalia">Somalia</option>
                              <option value="South Africa">South Africa</option>
                              <option value="South Georgia">South Georgia and the South Sandwich Islands</option>
                              <option value="Span">Spain</option>
                              <option value="SriLanka">Sri Lanka</option>
                              <option value="St. Helena">St. Helena</option>
                              <option value="St. Pierre and Miguelon">St. Pierre and Miquelon</option>
                              <option value="Sudan">Sudan</option>
                              <option value="Suriname">Suriname</option>
                              <option value="Svalbard">Svalbard and Jan Mayen Islands</option>
                              <option value="Swaziland">Swaziland</option>
                              <option value="Sweden">Sweden</option>
                              <option value="Switzerland">Switzerland</option>
                              <option value="Syria">Syrian Arab Republic</option>
                              <option value="Taiwan">Taiwan, Province of China</option>
                              <option value="Tajikistan">Tajikistan</option>
                              <option value="Tanzania">Tanzania, United Republic of</option>
                              <option value="Thailand">Thailand</option>
                              <option value="Togo">Togo</option>
                              <option value="Tokelau">Tokelau</option>
                              <option value="Tonga">Tonga</option>
                              <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                              <option value="Tunisia">Tunisia</option>
                              <option value="Turkey">Turkey</option>
                              <option value="Turkmenistan">Turkmenistan</option>
                              <option value="Turks and Caicos">Turks and Caicos Islands</option>
                              <option value="Tuvalu">Tuvalu</option>
                              <option value="Uganda">Uganda</option>
                              <option value="Ukraine">Ukraine</option>
                              <option value="United Arab Emirates">United Arab Emirates</option>
                              <option value="United Kingdom">United Kingdom</option>
                              <option value="United States">United States</option>
                              <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                              <option value="Uruguay">Uruguay</option>
                              <option value="Uzbekistan">Uzbekistan</option>
                              <option value="Vanuatu">Vanuatu</option>
                              <option value="Venezuela">Venezuela</option>
                              <option value="Vietnam">Viet Nam</option>
                              <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                              <option value="Virgin Islands (U.S)">Virgin Islands (U.S.)</option>
                              <option value="Wallis and Futana Islands">Wallis and Futuna Islands</option>
                              <option value="Western Sahara">Western Sahara</option>
                              <option value="Yemen">Yemen</option>
                              <option value="Yugoslavia">Yugoslavia</option>
                              <option value="Zambia">Zambia</option>
                              <option value="Zimbabwe">Zimbabwe</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* country */}
                    {/* email */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right">
                        <label className="control-label label-color">E-Mail :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="email" required="true" name="email" value={this.state.email} onChange={this.handleInputChange} className="form-control" />
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* email */}
                    {/* mobile number */}
                    <div className="row margin-tb-15">
                      <div className="col-sm-4 text-align-right padding-right-5">
                        <label className="control-label label-color" >Mobile Number :</label>
                      </div>
                      <div className="col-sm-8 margin-padding-0">
                        <div className="input-group display-block">
                          <div className="form-group label-floating">
                            <input type="text" name="mobile_number" required="true" value={this.state.mobile_number} onChange={this.handleInputChange} className="form-control" />
                            <span style={{ color: "red" }}>{this.state.errors["mobile_number"]}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/*mobile number  */}
                  </div>
                  {/* Photo */}
                  <div className="col-sm-6">
                    <div className="row">
                      <div className="col-sm-3"></div>
                      <div className="col-sm-6">
                        <button type="button" className="btn btn-link btn-twitter" onClick={() => { this.refs.photoGuidelines.show() }}>
                          <i className="material-icons">info_outline</i> Photo Guidelines
                       </button>
                      </div>
                      <div className="col-sm-3"></div>
                    </div>

                    <div className="row">
                      <div className="col-sm-12 margin-lr-10">
                        <KreedImage theKey="photo" setLoading={this.markUploaderStart} value={this.state.photo} width="350px" height="400px" text="Upload ID Card Photo" updateAction={this.updateAction} />
                      </div>
                    </div>
                  </div>
                  {/* Photo */}
                  <div className="col-sm-1"></div>
                  <div className="row margin-top-25px">
                    <div className="col-sm-12 text-align-center">
                      <button className="btn btn-rose btn-size font-size" value="Save"> <i className="material-icons">save</i> Save</button>
                      <button onClick={this.cancelAddNewItem} className="btn btn-rose btn-size font-size " value="Cancel"> <i className="material-icons">cancel</i> Cancel</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </SkyLight>
        {/*end pp_add*/}
        {/*End Skylight for adding new coach */}

        {/* Skylight for photo Guidelines */}
        <SkyLight dialogStyles={photoGuidelines} hideOnOverlayClicked ref="photoGuidelines">

          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">Photo Guidelines</h4>
            </div>
            <div className="modal-body">
              <ol className="ol-modal-styling">
                <li>The picture you are uploading should have a minimum 300 dpi, or a minimum of roughly 400 pixels x 400 pixels. A common resolution which is available on most cameras is 640 x 480 pixels.</li>
                <li>If you are taking a photo with your mobile phone, please ensure a quality setting of ‘high’ or ‘medium’. The higher the resolution, the better the outcome is likely to be!</li>
                <li>Please take either a full face view (head-on) or a ¾ view (shoulders slightly turned left or right, but with the head facing the camera)</li>
                <li>Lighting is very important; lighting should be uniform and bright, without casting shadows. Natural light is best, but if you are using artificial lighting be sure it comes from several sources. </li>
                <li>If you do not have natural lighting available, halogen or incandescent light works best. Fluorescent lighting should be avoided.</li>
                <li>Backgrounds are best when they are uniform and neutral: white or off-white is the most common. Solid blue or green backgrounds are often used by professionals, as these can be easily masked out. Make sure the background fills the full frame of the picture behind the person.</li>
                <li>Please ensure you upload photos in .jpg or .png formats only.</li>
              </ol>
            </div>
            <div className="modal-footer">
              <button onClick={this.cancelphotoGuidelines} className="btn btn-rose btn-size font-size " value="Cancel">Close</button>
            </div>
          </div>
        </SkyLight>
        {/* end Skylight forphoto Guidelines */}
        <SkyLight hideOnOverlayClicked ref="simpleDialog" title="">
          <span><h3 className="center-block">Add new key</h3></span>
          <br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Name of they key</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="NAME_OF_THE_NEW_KEY" value={"name"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div><br /><br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Value</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="VALUE_OF_THE_NEW_KEY" value={"value"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div>
          <div className="col-sm-12 ">
            <div className="col-sm-3 ">
            </div>
            <div className="col-sm-6 center-block">
              <a onClick={this.addKey} className="btn btn-rose btn-round center-block"><i className="fa fa-save"></i>   Add key</a>
            </div>
            <div className="col-sm-3 ">
            </div>
          </div>
        </SkyLight>
      </div>
    )
  }

}
export default CoachAdmin;

