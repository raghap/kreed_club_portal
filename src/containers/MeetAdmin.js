import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom';
import { Link } from 'react-router'
import firebase from '../config/database'
import Fields from '../components/fields/Fields.js'
import Input from '../components/fields/Input.js';
import Image from '../components/fields/Image.js';
import Table from '../components/tables/Table.js';
import SimpleTable from '../components/tables/SimpleTable.js';
import FileUploader from 'react-firebase-file-uploader';
import Indicator from '../components/Indicator'

// import 'babel-polyfill';
import {
  Accordion,
  AccordionItem,
  AccordionItemTitle,
  AccordionItemBody,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';

import Config from '../config/app';
import Common from '../common.js';
import Notification from '../components/Notification';
import SkyLight from 'react-skylight';
import INSERT_STRUCTURE from "../config/firestoreschema.js"
import FirebasePaginator from "firebase-paginator"
import NavBar from '../components/NavBar'
import moment from 'moment';
import SweetAlert from 'react-bootstrap-sweetalert';
import DateTime from '../components/fields/DateTime.js';
import Select from '../components/fields/Select.js';
// import SelectVenue from '../components/fields/SelectVenue.js';
import * as firebaseREF from 'firebase';
require("firebase/firestore");

const ROUTER_PATH = "/meets/";
var Loader = require('halogen/PulseLoader');

class MeetAdmin extends Component {

  constructor(props) {
    super(props);

    //Create initial step
    this.state = {
      meets: [],
      sessions: [],
      events: [],
      documents: [],
      collections: [],
      currentCollectionName: "",
      isCollection: false,
      isDocument: false,
      keyToDelete: null,
      pathToDelete: null,
      theSubLink: null,
      fieldsOfOnsert: null,
      isLoading: true,
      swapLoading: false,
      showAddCollection: "",

      debounce: false,
      imageLoading: false,
      meet_name: "",
      from: "",
      to: "",
      course: "Short(25m)",
      other_course: "",
      venue: "",
      max_events: "",
      links:[],
      avatarURL:"",
      isUploading:false,
      dlProgress: new Map(),

      country: "India",
      session_name: "",

      distance: "100m",
      stroke: "Free Style",
      category: "Boys/Men",
      swim_group: "I",
      event_number: "",

      editedFields: [],
      errors: {},


      itemToDelete: "event",
      phoneAlert: false,
      loadingAlert: false,
      deleteAlert: false,
      dateAlert: false,
      links: []

    };

    //Bind function to this
    this.getCollectionDataFromFireStore = this.getCollectionDataFromFireStore.bind(this);
    this.getMeets = this.getMeets.bind(this);
    this.resetDataFunction = this.resetDataFunction.bind(this);
    this.processRecords = this.processRecords.bind(this);
    this.updateAction = this.updateAction.bind(this);
    this.cancelDelete = this.cancelDelete.bind(this);
    this.cancelAddFirstItem = this.cancelAddFirstItem.bind(this);
    this.doDelete = this.doDelete.bind(this);
    this.deleteFieldAction = this.deleteFieldAction.bind(this);
    this.refreshDataAndHideNotification = this.refreshDataAndHideNotification.bind(this);
    this.addKey = this.addKey.bind(this);
    this.showSubItems = this.showSubItems.bind(this);
    this.updatePartOfObject = this.updatePartOfObject.bind(this);
    this.addDocumentToCollection = this.addDocumentToCollection.bind(this);
    this.addItemToArray = this.addItemToArray.bind(this);


    // this.formValueCapture=this.formValueCapture.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.validateInputs = this.validateInputs.bind(this);
    this.saveEdits = this.saveEdits.bind(this);
    this.resetEdits = this.resetEdits.bind(this);
    this.markUploaderStart = this.markUploaderStart.bind(this);

    //Alerts related
    this.hideAlert = this.hideAlert.bind(this);
    this.showAlert = this.showAlert.bind(this);

    // for hiding add new meet skylight
    this.listMeets = this.listMeets.bind(this);

    this.addEventBtnHandler = this.addEventBtnHandler.bind(this);
    this.refreshEvents = this.refreshEvents.bind(this);


    this.cancelAlertDelete = this.cancelAlertDelete.bind(this);
    //to swap events
    this.swapSequence = this.swapSequence.bind(this);

    //documents
    this.displayList = this.displayList.bind(this);
    this.handleUploadStart = this.handleUploadStart.bind(this);
    this.handleProgress = this.handleProgress.bind(this);
    this.handleUploadError = this.handleUploadError.bind(this);
    this.handleUploadSuccess = this.handleUploadSuccess.bind(this);
  }
    // for diplaying file list
    handleUploadStart(fname, task) {
      this.state.dlProgress.set(task.snapshot.ref.fullPath, 0);
      if (!this.state.isUploading)
        this.setState({ isUploading: true, progress: 0 });
    }
    handleProgress(progress, task) {
      if (progress <= 100) {
        this.state.dlProgress.set(task.snapshot.ref.fullPath, progress)
        this.setState({ avatarURL: task.snapshot.ref.fullPath });
      }
  
    }
    handleUploadError(error) {
      this.setState({ isUploading: false });
      console.error(error);
    }
    handleUploadSuccess(filename, task) {
      // this.setState({avatar: filename, progress: 100, isUploading: false});
      this.state.dlProgress.delete(task.snapshot.ref.fullPath);
      if (this.state.dlProgress.size == 0)
        this.state.isUploading = false;

      firebase.app.storage().ref('meet_docs').child(filename).getDownloadURL()
        .then(url => {
          this.state.links.push(url);
          this.setState({ avatarURL: url });
        });
    };

  displayList() {
    var i;
    var items = [];
    for (i = 0; i < this.state.links.length; i++) {
      var link = this.state.links[i];
      var pts = link.split('/');
      var fn = unescape(pts[pts.length - 1].split('?')[0]);
      var fnparts = fn.split('/');
      items.push(<li>{fnparts[1]}</li>);
    }
    return items;
  }
  cancelAlertDelete() {
    this.setState({ deleteAlert: false });
    this.state.itemToDelete = "event";
  }

  //sweet alerts for delete data
  showAlert(name) {
    this.setState({ [name]: true });
  }
  hideAlert(name) {
    this.setState({ [name]: false });
  }

  /**
   * Step 0a
   * Start getting data
   */
  componentDidMount() {
    this.findFirestorePath();
    window.sidebarInit();
  }

  /**
  * Step 0b
  * Resets data function
  */
  resetDataFunction() {
    var newState = {};
    newState.documents = [];
    newState.collections = [];
    newState.currentCollectionName = "";
    newState.fieldsAsArray = [];
    newState.arrayNames = [];
    newState.fields = [];
    newState.arrays = [];
    newState.elements = [];
    newState.elementsInArray = [];
    newState.theSubLink = null;

    newState.meet_name = "";
    newState.venue = "";
    newState.from = "";
    newState.to = "";
    newState.course = "Short(25m)";
    newState.other_course = "";
    newState.max_events = "";
    newState.links = [];
    this.state.dlProgress.clear();
    newState.session_name = "";
 

    newState.distance = "100m";
    newState.stroke = "Free Style";
    newState.category = "Boys/Men";
    newState.swim_group = "I";
    newState.event_number = "";
    //newState.editedFields = [];
    this.resetEdits();

    this.setState(newState);
    this.findFirestorePath();
  }

  /**
   * Step 0c
   * componentWillReceiveProps event of React, fires when component is mounted and ready to display
   * Start connection to firebase
   */
  componentWillReceiveProps(nextProps, nextState) {
    console.log("Next SUB: " + nextProps.params.sub);
    console.log("Prev SUB : " + this.props.params.sub);
    if (nextProps.params.sub == this.props.params.sub) {
      console.log("update now");
      this.setState({ isLoading: true })
      this.resetDataFunction();
    }
  }

  /**
   * Step 0d
   * getMeTheFirestorePath created firestore path based on the router parh
   */
  getMeTheFirestorePath() {
     var thePath = (this.props.route.path.replace(ROUTER_PATH, "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");;
    return thePath;
  }
  


  /**
   * Step 1
   * Finds out the Firestore path
   * Also creates the path that will be used to access the insert
   */
  findFirestorePath() {
    var pathData = {}
    if (this.props.params && this.props.params.sub) {
      pathData.lastSub = this.props.params.sub;
    }

    //Find the firestore path
    var firebasePath = this.getMeTheFirestorePath();
    pathData.firebasePath = firebasePath;

    //Find last path - the last item
    var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
    var items = subPath.split(Config.adminConfig.urlSeparator);
    pathData.lastPathItem = Common.capitalizeFirstLetter(items[items.length - 1]);
    pathData.completePath = subPath;

    //Save this in state
    this.setState(pathData);

  
    //Go to next step of finding the collection data
    this.getCollectionDataFromFireStore(firebasePath);
    
       // to get all enrollemets
       var db = firebase.app.firestore();
       var enrolledEvents = []
       var top_path = "aquaticsID/India/associations/KSA/enrollments";
       db.collection(top_path).get()
       .then(querySnapshot => {
 
           querySnapshot.forEach(function (doc) {
             var vals = doc.data();
                 enrolledEvents.push(vals);    
           })
           this.setState({ enrolledEvents: enrolledEvents });
       })
       .catch(error => {
         console.log("Error getting coach list: ", error);
       })
       this.userId = firebase.app.auth().currentUser.uid;
       var db = firebase.app.firestore();
       var docRef = db.collection("clubmaps").doc("user" + this.userId);
       var athletePath;
       docRef.get().then(doc => {
         if (doc.exists) {
           athletePath = doc.data().dbpath;
            // to get all athletes
       var athletesList = []
       var top_path =  athletePath + "/athletes";
       db.collection(top_path).get()
       .then(querySnapshot => {
 
           querySnapshot.forEach(function (doc) {
             var vals = doc.data();
               athletesList.push(vals);    
           })
           this.setState({ athletesList: athletesList });
       })
       .catch(error => {
         console.log("Error getting coach list: ", error);
       })

      }
         else { alert("error"); }
       })

      //  //-----Block of code to get the list of coaches in this club
      //  var venues = [];
      //  var db = firebase.app.firestore();
      //  pathData.isLoading = true;
      //  var ccoll = pathData.firebasePath.replace(/\/[^\/]*$/, '/venues');
      //  db.collection(ccoll).get()
      //    .then(querySnapshot => {
      //      querySnapshot.forEach(function (doc) {
      //        var vals = doc.data();
      //        venues.push(vals.name);
      //      })
      //      this.setState({ meetVenues: venues });
      //    })
      //    .catch(error => {
      //      console.log("Error getting coach list: ", error);
      //    })
    //this.getMeets();
  }

  swapSequence(event1, event2) {
    this.setState({ swapLoading: true });
    var eparts = event1.split('/');
    const m_id = eparts[0];
    const s_id = eparts[2];

    const dbpath = "aquaticsID/India/associations/KSA/meets/";
    var seq1, seq2;

    var db = firebase.app.firestore();

    var docref1 = db.doc(dbpath + event1);
    var docref2 = db.doc(dbpath + event2);

    docref1.get()
      .then(snap1 => {
        seq1 = snap1.data().sequence;
        return docref2.get();
      })
      .then(snap2 => {
        seq2 = snap2.data().sequence;
        return docref1.update({ sequence: seq2 });
      })
      .then(someval => {
        return docref2.update({ sequence: seq1 });
      })
      .then(someval => {
        this.refreshEvents(m_id, s_id, false);
      })
      .catch(err => {
        console.log("Error in swap: " + err);
      })

  }

  getMeets() {
    var db = firebase.app.firestore();
    var top_path = "aquaticsID/India/associations/KSA/meets";

    var lmeets = [];
    var lsessions = [];
    var levents = [];
    var tasks = [], etasks = [];

    var _this = this;

    db.collection(top_path).orderBy("from").get()
      .then(function (meet_snap) {

        meet_snap.forEach(function (meet_doc) {
          var doc = meet_doc.data();
          doc.meet_id = meet_doc.id;
          lmeets.push({ id: meet_doc.id, data: doc });

          tasks.push(db.collection(top_path + "/" + meet_doc.id + "/sessions").get());
        });
      })
      .then(someval => {

        for (var subtask of tasks) {

          subtask.then(function (session_snap) {
            session_snap.forEach(function (session_doc) {

              var sdoc = session_doc.data();

              sdoc.session_id = session_doc.id;
              sdoc.meet_id = session_doc.ref.parent.parent.id;
              lsessions.push({ id: session_doc.id, data: sdoc });

              etasks.push(db.collection(top_path + "/" + sdoc.meet_id + "/sessions/" + session_doc.id + "/events").orderBy("sequence").get());

            });
          });
        }
        return Promise.all(tasks);
      })
      .then(someval => {

        for (var etask of etasks) {
          etask.then(function (event_snap) {
            event_snap.forEach(function (event_doc) {
              var edoc = event_doc.data();
              edoc.event_id = event_doc.id;
              edoc.session_id = event_doc.ref.parent.parent.id;
              edoc.meet_id = event_doc.ref.parent.parent.parent.parent.id;
              edoc.uidOfFirebase = edoc.meet_id + "+sessions+" + edoc.session_id + "+events+" + edoc.event_id;
              levents.push({ id: event_doc.id, data: edoc });
            });

          });
        }

        return Promise.all(etasks);
      })
      .then(someval => {
        // alert("sessions: "+lsessions.length+" events: "+levents.length);
        _this.setState({ isLoading: false, isCollection: true, meets: lmeets, sessions: lsessions, events: levents });
      });


  }
  /**
  * Step 2
  * Connect to firestore to get the current item we need
  * @param {String} collection - this infact can be collection or document
  */
  getCollectionDataFromFireStore(collection) {
    //Create the segmments based on the path / collection we have
    var segments = collection.split("/");
    var lastSegment = segments[segments.length - 1];

    //Is this a call to a collections data
    var isCollection = segments.length % 2;

    //Reference to this
    var _this = this;

    //Save know info for now
    this.setState({
      currentCollectionName: segments[segments.length - 1],
      isCollection: isCollection,
      isDocument: !isCollection,
    })

    //Get reference to firestore
    var db = firebase.app.firestore();

    //Here, we will save the documents from collection
    var documents = [];

    if (isCollection) {
      //COLLECTIONS - GET DOCUMENTS 
      this.getMeets();

    } else {
      //DOCUMENT - GET FIELDS && COLLECTIONS
      var referenceToCollection = collection.replace("/" + lastSegment, "");

      //Create reference to the document itseld
      //alert(referenceToCollection + "///" + lastSegment);

      var docRef = db.collection(referenceToCollection).doc(lastSegment);

      //Get the starting collectoin
      var parrentCollection = segments;
      parrentCollection.splice(-1, 1);

      //Find the collections of this document
      this.findDocumentCollections(parrentCollection);

      docRef.get().then(function (doc) {
        if (doc.exists) {
          console.log("Document data:", doc.data());
          //Directly process the data
          _this.processRecords(doc.data())
        } else {
          console.log("No such document!");
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
      });
    }
  }

  /**
   * Step 3
   * findDocumentCollections - what collections should we display / currently there is no way to get collection form docuemnt
   * @param {Array} chunks - the collection / documents
   */
  findDocumentCollections(chunks) {
    console.log("Search for the schema now of " + chunks);

    //At start is the complete schema
    var theInsertSchemaObject = INSERT_STRUCTURE;
    var cuurrentFields = null;
    console.log("CHUNKS");
    console.log(chunks);

    //Foreach chunks, find the collections / fields
    chunks.map((item, index) => {
      console.log("current chunk:" + item);

      //Also make the last object any
      //In the process, check if we have each element in our schema
      if (theInsertSchemaObject != null && theInsertSchemaObject && theInsertSchemaObject[item] && theInsertSchemaObject[item]['collections']) {
        var isLastObject = (index == (chunks.length - 1));

        if (isLastObject && theInsertSchemaObject != null && theInsertSchemaObject[item] && theInsertSchemaObject[item]['fields']) {
          cuurrentFields = theInsertSchemaObject[item]['fields'];
        }

        if (isLastObject && theInsertSchemaObject != null) {
          //It is last
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        } else {
          theInsertSchemaObject = theInsertSchemaObject[item]['collections'];
        }
      } else {
        theInsertSchemaObject = [];
      }
      console.log("Current schema");
      console.log(theInsertSchemaObject);


    })

    //Save the collection to be shown as button and fieldsOfOnsert that will be used on inserting object
    this.setState({ collections: theInsertSchemaObject, fieldsOfOnsert: cuurrentFields })
  }

  /**
   * Step 4
   * Processes received records from firebase
   * @param {Object} records
   */
  processRecords(records) {
    console.log(records);

    var fields = {};
    var arrays = {};
    var elements = [];
    var elementsInArray = [];
    var newState = {};
    var directValue = "";
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.elements = elements;
    newState.directValue = directValue;
    newState.elementsInArray = elementsInArray;
    newState.records = null;

    this.setState(newState);

    //Each display is consisted of
    //Fields   - This are string, numbers, photos, dates etc...
    //Arrays   - Arrays of data, ex items:[0:{},1:{},2:{}...]
    //         - Or object with prefixes that match in array
    //Elements - Object that don't match in any prefix for Join - They are represented as buttons.

    //In FireStore
    //GeoPoint
    //DocumentReference

    //If record is of type array , then there is no need for parsing, just directly add the record in the arrays list
    if (Common.getClass(records) == "Array") {
      //Get the last name
      console.log("This is array");
      var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : ""
      var allPathItems = subPath.split("+");
      console.log(allPathItems)
      if (allPathItems.length > 0) {
        var lastItem = allPathItems[allPathItems.length - 1];
        console.log(lastItem);
        arrays[lastItem] = records;

      }
      //this.setState({"arrays":this.state.arrays.push(records)})
    } else if (Common.getClass(records) == "Object") {
      //Parse the Object record
      for (var key in records) {
        if (records.hasOwnProperty(key)) {
          var currentElementClasss = Common.getClass(records[key]);
          console.log(key + "'s class is: " + currentElementClasss);

          //Add the items by their type
          if (currentElementClasss == "Array") {
            //Add it in the arrays  list
            arrays[key] = records[key];
          } else if (currentElementClasss == "Object") {
            //Add it in the elements list
            var isElementMentForTheArray = false; //Do we have to put this object in the array
            for (var i = 0; i < Config.adminConfig.prefixForJoin.length; i++) {
              if (key.indexOf(Config.adminConfig.prefixForJoin[i]) > -1) {
                isElementMentForTheArray = true;
                break;
              }
            }

            var objToInsert = records[key];
            objToInsert.uidOfFirebase = key;

            if (isElementMentForTheArray) {
              //Add this to the merged elements
              elementsInArray.push(objToInsert);
            } else {
              //Add just to elements
              elements.push(objToInsert);
            }

          } else if (currentElementClasss != "undefined" && currentElementClasss != "null") {
            //This is string, number, or Boolean
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "GeoPoint") {
            //This is GeoPOint
            //Add it to the fields list
            fields[key] = records[key];
          } else if (currentElementClasss == "DocumentReference") {
            //This is DocumentReference
            //Add it to the fields list
            fields[key] = records[key];
          }

        }
      }
    } if (Common.getClass(records) == "String") {
      console.log("We have direct value of string");
      directValue = records;
    }

    //Convert fields from object to array
    var fieldsAsArray = [];
    console.log("Add the items now inside fieldsAsArray");
    console.log("Current schema");
    console.log(this.state.currentInsertStructure)
    //currentInsertStructure
    var keysFromFirebase = Object.keys(fields);
    console.log("keysFromFirebase")
    console.log(keysFromFirebase)
    var keysFromSchema = Object.keys(this.state.currentInsertStructure || {});
    console.log("keysFromSchema")
    console.log(keysFromSchema)

    keysFromSchema.forEach((key) => {
      if (fields.hasOwnProperty(key)) {
        fieldsAsArray.push({ "theKey": key, "value": fields[key] })
        var indexOfElementInFirebaseObject = keysFromFirebase.indexOf(key);
        if (indexOfElementInFirebaseObject > -1) {
          keysFromFirebase.splice(indexOfElementInFirebaseObject, 1);
        }
      }
    });

    console.log("keysFromFirebase")
    console.log(keysFromFirebase)


    keysFromFirebase.forEach((key) => {
      if (fields.hasOwnProperty(key)) {
        fieldsAsArray.push({ "theKey": key, "value": fields[key] })
      }
    });

    //Get all array names
    var arrayNames = [];
    Object.keys(arrays).forEach((key) => {
      arrayNames.push(key)
    });

    var newState = {};
    newState.fieldsAsArray = fieldsAsArray;
    newState.arrayNames = arrayNames;
    newState.fields = fields;
    newState.arrays = arrays;
    newState.isJustArray = Common.getClass(records) == "Array";
    newState.elements = elements;
    newState.elementsInArray = elementsInArray;
    newState.directValue = directValue;
    newState.records = records;
    newState.isLoading = false;

    console.log("THE elements")
    console.log(elements);

    //Set the new state
    this.setState(newState);

    //Additional init, set the DataTime, check format if something goes wrong
    window.additionalInit();
  }

  /**
   *
   * Create R Update D
   *
   */

  /**
  * processValueToSave  - helper for saving in Firestore , converts value to correct format
  * @param {value} value
  * @param {type} type of field
  */
  processValueToSave(value, type) {
    //To handle number values
    if (!isNaN(value)) {
      value = Number(value);
    }

    //To handle boolean values
    value = value === "true" ? true : (value === "false" ? false : value);


    if (type == "date") {
      //To handle date values
      if (moment(value).isValid()) {
        value = moment(value).toDate();
        //futureStartAtDate = new Date(moment().locale("en").add(1, 'd').format("MMM DD, YYYY HH:MM"))
      }
    }

    return value;
  }

  /**
  * updatePartOfObject  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {String} firebasePath current firestore path
  * @param {String} byGivvenSubLink force link to field
  * @param {Function} callback function after action
  */
  updatePartOfObject(key, value, dorefresh = false, type = null, firebasePath, byGivvenSubLink = null, callback = null) {
    var subLink = this.state.theSubLink;
    if (byGivvenSubLink != null) {
      subLink = byGivvenSubLink;
    }
    console.log("Sub save " + key + " to " + value + " and the path is " + firebasePath + " and theSubLink is " + subLink);
    var chunks = subLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
    var _this = this;
    //First get the document
    //DOCUMENT - GET FIELDS && COLLECTIONS
    var docRef = firebase.app.firestore().doc(firebasePath);
    docRef.get().then(function (doc) {
      if (doc.exists) {
        var numChunks = chunks.length - 1;
        var doc = doc.data();
        if (value == "DELETE_VALUE") {
          if (numChunks == 2) {
            doc[chunks[1]].splice(chunks[2], 1);
          }
          if (numChunks == 1) {
            doc[chunks[1]] = null;
          }
        } else {
          //Normal update, or insert
          if (numChunks == 2) {
            doc[chunks[1]][chunks[2]] = value
          }
          if (numChunks == 1) {
            doc[chunks[1]][key] = value
          }
        }

        console.log("Document data:", doc);
        _this.updateAction(chunks[1], doc[chunks[1]], dorefresh, null, true)
        if (callback) {
          callback();
        }

        //alert(chunks.length-1);
        //_this.processRecords(doc.data())
        //console.log(doc);

      } else {
        console.log("No such document!");
      }
    }).catch(function (error) {
      console.log("Error getting document:", error);
    });

  }



  /**
  * Firebase update based on key / value,
  * This function also sets derect name and value
  * @param {String} key
  * @param {String} value
  */

  /**
  * updateAction  - updates sub data from document in firestore, this also does Delete
  * @param {String} key to be updated
  * @param {String} value
  * @param {Boolean} refresh after action
  * @param {String} type of file
  * @param {Boolean} forceObjectSave force saving sub object
  */
  updateAction(key, value, dorefresh = false, type = null, forceObjectSave = false) {
    value = this.processValueToSave(value, type);
    var firebasePath = (this.props.route.path.replace("/assoc/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (this.state.theSubLink != null && !forceObjectSave) {
      this.updatePartOfObject(key, value, dorefresh, type, firebasePath)
    } else {

      //value=firebase.firestore().doc("/users/A2sWwzDop0EAMdfxfJ56");
      //key="creator";

      console.log("firebasePath from update:" + firebasePath)
      console.log('Update ' + key + " into " + value);

      if (key == "NAME_OF_THE_NEW_KEY" || key == "VALUE_OF_THE_NEW_KEY") {
        console.log("THE_NEW_KEY")
        var updateObj = {};
        updateObj[key] = value;
        this.setState(updateObj);
        console.log(updateObj);
      } else {

        //PP: DONT UPDATE THE DB HERE. WAIT FOR THE SAVE
        if (!this.state.editedFields.includes(key))
          this.state.editedFields.push(key);

        if (key == "photo")
          this.state.imageLoading = false;

        this.setState({ [key]: value });

      }

    }
  }

  resetEdits() {
    this.state.editedFields.splice(0, this.state.editedFields.length);
  }

  saveEdits() {
    var firebasePath = (this.props.route.path.replace("/meets/", "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    var db = firebase.app.firestore();

    var databaseRef = db.doc(firebasePath);
    var updateObj = {};
    var i;
    for (i = this.state.editedFields.length - 1; i >= 0; --i) {
      updateObj[this.state.editedFields[i]] = this.state[this.state.editedFields[i]];
      this.state.editedFields.splice(i, 1);
    }
    databaseRef.set(updateObj, { merge: true });

  }


  /**
  * addDocumentToCollection  - used recursivly to add collection's document's collections
  * @param {String} name name of the collection
  * @param {FirestoreReference} reference
  */
  addDocumentToCollection(name, reference = null) {

    var pathChunks = this.state.firebasePath.split("/");
    pathChunks.pop();
    var withoutLast = pathChunks.join("/");
    console.log(name + " vs " + withoutLast);
    //Find the fields to be inserted
    var theInsertSchemaObject = INSERT_STRUCTURE[name].fields;
    console.log(JSON.stringify(theInsertSchemaObject));

    //Find the collections to be inserted
    var theInsertSchemaCollections = INSERT_STRUCTURE[name].collections;
    console.log(JSON.stringify(theInsertSchemaCollections));

    //Reference to root firestore or existing document reference
    var db = reference == null ? (pathChunks.length > 1 ? firebase.app.firestore().doc(withoutLast) : firebase.app.firestore()) : reference;

    //Check type of insert
    var isTimestamp = Config.adminConfig.methodOfInsertingNewObjects == "timestamp"

    //Create new element
    var newElementRef = isTimestamp ? db.collection(name).doc(Date.now()) : db.collection(name).doc()

    //Add data to the new element
    //newElementRef.set(theInsertSchemaObject)

    //Go over sub collection and insert them
    for (var i = 0; i < theInsertSchemaCollections.length; i++) {
      this.addDocumentToCollection(theInsertSchemaCollections[i], newElementRef)
    }


    //Show the notification on root element
    if (reference == null) {
      this.cancelAddFirstItem();
      this.setState({ notifications: [{ type: "success", content: "Element added. You can find it in the table below." }] });
      this.refreshDataAndHideNotification();
    }
  }

  /**
  * addKey
  * Adds key in our list of fields in firestore
  */
  addKey() {
    if (this.state.NAME_OF_THE_NEW_KEY && this.state.NAME_OF_THE_NEW_KEY.length > 0) {

      if (this.state.VALUE_OF_THE_NEW_KEY && this.state.VALUE_OF_THE_NEW_KEY.length > 0) {

        this.setState({ notifications: [{ type: "success", content: "New key added." }] });
        this.updateAction(this.state.NAME_OF_THE_NEW_KEY, this.state.VALUE_OF_THE_NEW_KEY);
        this.refs.simpleDialog.hide()
        this.refreshDataAndHideNotification();
      }
    }
  }

  /**
  * addItemToArray  - add item to array
  * @param {String} name name of the array
  * @param {Number} howLongItIs count of items, to know the next index
  */
  addItemToArray(name, howLongItIs) {
    console.log("Add item to array " + name);
    console.log("Is just array " + this.state.isJustArray);

    console.log("Data ");
    console.log(this.state.fieldsOfOnsert);

    var dataToInsert = null;
    var correctPathToInsertIn = "";
    if (this.state.fieldsOfOnsert) {
      if (this.state.isJustArray) {
        console.log("THIS IS Array")
        dataToInsert = this.state.fieldsOfOnsert[0];
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      } else {
        dataToInsert = this.state.fieldsOfOnsert[name];
        dataToInsert = dataToInsert ? dataToInsert[0] : null;
        correctPathToInsertIn = this.state.firebasePath + Config.adminConfig.urlSeparatorFirestoreSubArray + name + Config.adminConfig.urlSeparatorFirestoreSubArray + (parseInt(howLongItIs));
      }
    }

    console.log("Data to insert");
    console.log(dataToInsert);
    console.log("Path to insert");
    console.log(correctPathToInsertIn);

    var _this = this;
    this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", dataToInsert, true, null, this.state.firebasePath, correctPathToInsertIn, function (e) {
      _this.setState({ notifications: [{ type: "success", content: "New element added." }] });
      _this.refreshDataAndHideNotification();
    })
  }

  /**
  *
  * C Read U D
  *
  */

  /**
  * showSubItems - displays sub object, mimics opening of new page
  * @param {String} theSubLink , direct link to the sub object
  */
  showSubItems(theSubLink) {
    var chunks = theSubLink.split(Config.adminConfig.urlSeparatorFirestoreSubArray);
    this.setState({
      itemOfInterest: chunks[1],
      theSubLink: theSubLink,
    })
    var items = this.state.records;
    for (var i = 1; i < chunks.length; i++) {
      console.log(chunks[i]);
      items = items[chunks[i]];
    }
    console.log("--- NEW ITEMS ");
    console.log(items)
    this.processRecords(items);
  }

  /**
  *
  * C R U Delete
  *
  */

  /**
  * deleteFieldAction - displays sub object, mimics opening of new page
  * @param {String} key to be updated
  * @param {Boolean} isItArrayItem 
  * @param {String} theLink 
  */
  deleteFieldAction(key, isItArrayItem = false, theLink = null) {
    if (theLink != null) {
      theLink = theLink.replace("/assoc", "");
    }
    if (isNaN(key)) {
      isItArrayItem = false;
    }
    console.log("Is it array: " + isItArrayItem);
    var firebasePathToDelete = (this.props.route.path.replace(ROUTER_PATH, "").replace(":sub", "")) + (this.props.params && this.props.params.sub ? this.props.params.sub : "").replace(/\+/g, "/");
    if (key != null) {
      //firebasePathToDelete+=("/"+key)
    }

    console.log("firebasePath for delete:" + firebasePathToDelete);
    this.setState({ pathToDelete: theLink ? theLink : firebasePathToDelete, isItArrayItemToDelete: isItArrayItem, keyToDelete: theLink ? "" : key });

    this.showAlert("deleteAlert");

  }

  /**
  * doDelete - do the actual deleting based on the data in the state
  */
  doDelete() {
    var _this = this;
    console.log("Do delete ");
    console.log("Is it array " + this.state.isItArrayItemToDelete);
    console.log("Path to delete: " + this.state.pathToDelete)
    var completeDeletePath = this.state.pathToDelete + "/" + this.state.keyToDelete;
    console.log("completeDeletePath to delete: " + completeDeletePath)
    if (this.state.pathToDelete.indexOf(Config.adminConfig.urlSeparatorFirestoreSubArray) > -1) {
      //Sub data
      //_this.refs.deleteDialog.hide();
      this.hideAlert("deleteAlert");

      this.updatePartOfObject("DIRECT_VALUE_OF_CURRENT_PATH", "DELETE_VALUE", true, null, this.state.firebasePath, this.state.pathToDelete, function (e) {
        _this.setState({ notifications: [{ type: "success", content: "Element deleted." }] });
        _this.refreshDataAndHideNotification();
      })
    } else {
      //Normal data

      var chunks = completeDeletePath.split("/");

      var db = firebase.app.firestore();


      if (chunks.length % 2) {
        //odd
        //Delete fields from docuemnt
        var refToDoc = db.doc(this.state.pathToDelete);

        // Remove the 'capital' field from the document
        var deleteAction = {};
        deleteAction[this.state.keyToDelete] = firebaseREF.firestore.FieldValue.delete();
        var removeKey = refToDoc.update(deleteAction).then(function () {
          console.log("Document successfully deleted!");
          //_this.refs.deleteDialog.hide();

          _this.setState({ deleteAlert: false, keyToDelete: null, pathToDelete: null, notifications: [{ type: "success", content: "Field is deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing document: ", error);
        });
      } else {
        //even
        //delete document from collection
        //alert("Delete document "+completeDeletePath);
        db.collection(this.state.pathToDelete).doc(this.state.keyToDelete).delete().then(function () {
          console.log("Document successfully deleted!");
          //_this.refs.deleteDialog.hide();

          _this.setState({ deleteAlert: false, pathToDelete: null, notifications: [{ type: "success", content: _this.state.itemToDelete + " is deleted." }] });
          _this.refreshDataAndHideNotification();
        }).catch(function (error) {
          console.error("Error removing document: ", error);
        });

      }

    }

    /*firebase.database().ref(this.state.pathToDelete).set(null).then((e)=>{
      console.log("Delete res: "+e)
      this.refs.deleteDialog.hide();
      this.setState({keyToDelete:null,pathToDelete:null,notifications:[{type:"success",content:"Field is deleted."}]});
      this.refreshDataAndHideNotification();

    })*/
  }

  /**
  * cancelDelete - user click on cancel
  */
  cancelDelete() {
    console.log("Cancel Delete");
    this.refs.deleteDialog.hide()
  }

  cancelAddFirstItem() {
    console.log("Cancel Add");
    this.refs.addCollectionDialog.hide()
  }


  handleInputChange(event) {
    const target = event.target;
    const name = target.name;
    const value = target.value;
    if (name == "mobile_number") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 10) {
        return false;
      }
    }

    if (name == "pin_code") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
      if (value.length > 6) {
        return false;
      }
    }

    if (name == "max_events") {
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
    }
    if(name == "event_number"){
      if (!value.match(/^[0-9]+$/) && value != '') {
        return false;
      }
    }
    
    this.setState({
      [name]: value
    });
  }

  validateInputs() {
    if (this.state.mobile_number.length < 10 || this.state.pin_code.length < 6)
      return false;

    return true;

  }

  markUploaderStart() {
    this.state.imageLoading = true;
  }

  refreshEvents(meetId, sessionId, notify = true) {
    var db = firebase.app.firestore();
    var top_path = "aquaticsID/India/associations/KSA/meets";
    var _this = this;

    if (!notify) {
      for (var idx = this.state.events.length - 1; idx >= 0; idx--) {
        var ev = this.state.events[idx].data;
        if (ev.meet_id == meetId && ev.session_id == sessionId)
          this.state.events.splice(idx, 1);

      }
    }

    var levents = this.state.events.slice();

    db.collection(top_path + "/" + meetId + "/sessions/" + sessionId + "/events").orderBy("sequence").get()
      .then(function (event_snap) {
        event_snap.forEach(function (event_doc) {
          var edoc = event_doc.data();
          edoc.event_id = event_doc.id;
          edoc.session_id = sessionId;
          edoc.meet_id = meetId;
          edoc.uidOfFirebase = edoc.meet_id + "+sessions+" + edoc.session_id + "+events+" + edoc.event_id;

          var event_exists = false;
          for (event of levents)
            if (event.id == event_doc.id)
              event_exists = true;
          if (!event_exists) {
            levents.push({ id: event_doc.id, data: edoc });
          }
        });
        //set state
        var shout_out = notify ? [{ type: "success", content: "New Event created. You can find it in the table below." }] : [];
        _this.setState({ events: levents, isCollection: true, debounce: false, notifications: shout_out, swapLoading: false })
      });
  }

  /**
  *
  * UI GENERATORS
  *
  */

  /**
  * This function finds the headers for the current menu
  * @param firebasePath - we will use current firebasePath to find the current menu
  */
  findHeadersBasedOnPath(firebasePath) {
    var headers = null;

    var itemFound = false;
    var navigation = Config.navigation;
    for (var i = 0; i < navigation.length && !itemFound; i++) {
      if (navigation[i].tableFields && navigation[i].link == "MeetAdmin") {
        headers = navigation[i].tableFields;
        itemFound = true;
      }

      //Look into the sub menus
      if (navigation[i].subMenus) {
        for (var j = 0; j < navigation[i].subMenus.length; j++) {
          if (navigation[i].subMenus[j].tableFields && navigation[i].subMenus[j].link == "MeetAdmin") {
            headers = navigation[i].subMenus[j].tableFields;
            itemFound = true;
          }
        }
      }
    }
    return headers;
  }

  findHeadersForEvents() {
    var headers = ["event_number","category","distance","stroke","swim_group"];
    return headers;
  }

  addEventBtnHandler(evt) {
    this.state.changeMeetId = evt.target.id.split("_")[0];
    this.state.changeSessionId = evt.target.id.split("_")[1];
    this.refs.addNewEventDialog.show();
    evt.stopPropagation()
  }

  listMeets() {
    var name = this.state.currentCollectionName;
    var name = "Meets";
    var component = [];

    for (var meet of this.state.meets) { //------> For each meet
      //****** Set the meet title *****

      var documents = [];
      if(meet.data.attachments.length >= 1){
        for(var i=0;i<meet.data.attachments.length;i++) {
          var link = meet.data.attachments[i];
          var pts = link.split('/');
          var fn = unescape(pts[pts.length - 1].split('?')[0]);
          var fnparts = fn.split('/');
          // alert(fnparts[0]+ " , " + fnparts[1]+ " , " + fnparts[2])
          documents.push(<li><a href={link} target="_blank">{fnparts[1]}</a></li>);
        }
      }

      var meet_title =
        <AccordionItemTitle>
          <div className="row">
            <div className="col-sm-9">
              <h3 className="u-position-relative">
                { meet.data.meet_name}
              </h3>
            </div>
            <div className="col-sm-3">
              <div className="accordion__arrow" role="presentation" />
            </div>
          </div>
          {/* <p>{meet.data.attachments}</p> */}
          <p>From: {meet.data.from.toLocaleString()}, TO: {meet.data.to.toLocaleString()}, Location: {meet.data.venue}</p>
          <p>{meet.data.description}</p>
        </AccordionItemTitle>;

    var event_docs = [];
    var event_bodies = [];
    var athlete_bodies = [];

    for (var event of this.state.events) //------> look through all events  
      if (event.data.meet_id == meet.id) //------> if event belongs to this event
        event_docs.push(event.data);

    event_bodies.push(
      <SimpleTable
        headers={this.findHeadersForEvents()}
        deleteFieldAction={this.deleteFieldAction}
        fromObjectInArray={true}
        name={name}
        routerPath={this.props.route.path}
        isJustArray={false}
        sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
        data={event_docs}
      />
    );
    
    //All enrollments to a meet 
    var meets_docs = [];
    for(var event of this.state.enrolledEvents){
      if( event.events[0].split('/')[0] == meet.id ){
        meets_docs.push(event);
      }
    }
    var athlete_docs = [];
    // get all athletes enrollred to a meet
    for(var meet of meets_docs){
      for(var athlete of this.state.athletesList){
        if(meet.Kreed_ID == athlete.Kreed_ID){
          var meet_name = [];
          for(var k=0; k<meet.events.length; k++){
            for(var event of this.state.events){
              if(meet.events[k].split('/')[2] == event.data.event_id){
                // alert(event.data.stroke + event.data.distance )
                meet_name.push(event.data.distance + " " + event.data.stroke + ", ");
              }
            }
          }
          athlete["Registered_events"] = meet_name;
          athlete_docs.push(athlete)
        }
      }
    }


    athlete_bodies.push(
      <SimpleTable
        headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
        deleteFieldAction={this.deleteFieldAction}
        fromObjectInArray={true}
        name={name}
        routerPath={this.props.route.path}
        isJustArray={false}
        sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
        data={athlete_docs}
      />
    );



      //Now push the entire meet into the component
      component.push(
        <Accordion>
          <AccordionItem>
            <AccordionItemTitle>{meet_title}</AccordionItemTitle>
            <AccordionItemBody>
            <p>Circular/Documents</p>
            <ol>{documents}</ol>
            {/* Events for the meet */}
            <Accordion>
              <AccordionItem>
                <AccordionItemTitle>
                <div className="row">
                    <div className="col-sm-9">
                      <h5 className="u-position-relative">
                        Events ({event_docs.length})
                      </h5>
                    </div>
                    <div className="col-sm-3">
                      <div className="accordion__arrow" role="presentation" />
                    </div>
                  </div>
                </AccordionItemTitle>
                <AccordionItemBody>
                  {event_bodies}
                </AccordionItemBody>
              </AccordionItem>
            </Accordion>

             {/* registered Atheltes for that meet */}
             <Accordion>
              <AccordionItem>
                <AccordionItemTitle>
                <div className="row">
                    <div className="col-sm-9">
                      <h5 className="u-position-relative">
                        Registered Swimmers ({athlete_docs.length})
                      </h5>
                    </div>
                    <div className="col-sm-3">
                      <div className="accordion__arrow" role="presentation" />
                    </div>
                  </div>
                </AccordionItemTitle>
                <AccordionItemBody>
                  {athlete_bodies}
                </AccordionItemBody>
              </AccordionItem>
            </Accordion>

            </AccordionItemBody>
          </AccordionItem>
        </Accordion>
      );

    }

    return (<div className="col-md-12" key={name}>
      <div className="card">
        <div className="card-header card-header-icon" data-background-color="rose">
          <i className="material-icons">assignment</i>
        </div>
        {/* <a style={{ cursor: "pointer" }} onClick={() => { this.refs.addnewitemDialog.show(); }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
          <i className="material-icons">add</i>
        </div></a> */}
        <div className="card-content">
          <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
          <div className="toolbar">
          </div>
          <div className="material-datatables">
            {component}
          </div>
        </div>
      </div>
    </div>);

  } //----- end listMeets -----


  /**
  * makeCollectionTable
  * Creates single collection documents
  */
  makeCollectionTable() {
    var name = this.state.currentCollectionName;
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          {/*begin pp_add*/}
          {/* <a style={{ cursor: "pointer" }} onClick={() => { this.refs.addnewitemDialog.show(); }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
          
            <i className="material-icons">add</i>
          </div></a> */}
          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                deleteFieldAction={this.deleteFieldAction}
                fromObjectInArray={true}
                name={name}
                routerPath={this.props.route.path}
                isJustArray={false}
                sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                data={this.state.documents}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
   * Creates single array section
   * @param {String} name, used as key also
   */
  makeArrayCard(name) {
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          <a onClick={() => { this.addItemToArray(name, this.state.arrays[name].length) }}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{ float: "right" }}>
            <i className="material-icons">add</i>
          </div></a>
          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                isFirestoreSubArray={true}
                showSubItems={this.showSubItems}
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)}
                deleteFieldAction={this.deleteFieldAction}
                fromObjectInArray={false} name={name}
                routerPath={this.props.route.path}
                isJustArray={this.state.isJustArray}
                sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""}
                data={this.state.arrays[name]} />
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
   * Creates  table section for the elements object
   * @param {String} name, used as key also
   */
  makeTableCardForElementsInArray() {
    var name = this.state.lastPathItem;
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>

          <div className="card-content">
            <h4 className="card-title">{Common.capitalizeFirstLetter(name)}</h4>
            <div className="toolbar">

            </div>
            <div className="material-datatables">
              <Table
                isFirestoreSubArray={true}
                showSubItems={this.showSubItems}
                headers={this.findHeadersBasedOnPath(this.state.firebasePath)} deleteFieldAction={this.deleteFieldAction} fromObjectInArray={true} name={name} routerPath={this.props.route.path} isJustArray={this.state.isJustArray} sub={this.props.params && this.props.params.sub ? this.props.params.sub : ""} data={this.state.elementsInArray}>
              </Table>
            </div>
          </div>
        </div>
      </div>
    )
  }

  /**
    * Creates direct value section
    * @param {String} value, valu of the current path
    */
  makeValueCard(value) {
    return (
      <div className="col-md-12" key={name}>
        <div className="card">
          <div className="card-header card-header-icon" data-background-color="rose">
            <i className="material-icons">assignment</i>
          </div>
          <div className="card-content">
            <h4 className="card-title">Value</h4>
            <div className="toolbar">
            </div>
            <div>
              <Input updateAction={this.updateAction} className="" theKey="DIRECT_VALUE_OF_CURRENT_PATH" value={value} />
            </div>
          </div>
        </div>
      </div>
    )
  }


  /**
   * generateBreadCrumb
   */
  generateBreadCrumb() {
    var subPath = this.props.params && this.props.params.sub ? this.props.params.sub : "";
    var items = subPath.split(Config.adminConfig.urlSeparator);
    var path = "/assoc/";
    return (<div>{items.map((item, index) => {
      if (index == 0) {
        path += item;
      } else {
        path += "+" + item;
      }

      return (<Link className="navbar-brand" to={path}>{item} <span className="breadcrumbSeparator">{index == items.length - 1 ? "" : "/"}</span><div className="ripple-container"></div></Link>)
    })}</div>)
  }

  /**
   * generateNotifications
   * @param {Object} item - notification to be created
   */
  generateNotifications(item) {
    return (
      <div className="col-md-12">
        <Notification type={item.type} >{item.content}</Notification>
      </div>
    )
  }

  /**
  * refreshDataAndHideNotification
  * @param {Boolean} refreshData 
  * @param {Number} time 
  */
  refreshDataAndHideNotification(refreshData = true, time = 3000) {
    //Refresh data,
    if (refreshData) {
      this.resetDataFunction();
    }

    //Hide notifications
    setTimeout(function () { this.setState({ notifications: [] }) }.bind(this), time);
  }

  //MAIN RENDER FUNCTION 
  render() {
    //begin pp_add
    var addnewitemDialog = {
      width: '50%',
      height: 'auto',
      marginLeft: '-25%',
      // marginTop:'-20%',
      position: 'absolute',
      padding: '0px'
    };
    //end pp_add
    return (
      <div className="content">
        {/*<NavBar>{this.generateBreadCrumb()}</NavBar>*/}
        <NavBar>
          <div className="pull-right">
            <a href="http://karnatakaswimming.org" target="_blank"> <img className="img-circle" src='assets/img/ksa_logo.png' height="70" width="80" /></a>
          </div>
        </NavBar>


        <div className="content" sub={this.state.lastSub}>

          <div className="container-fluid">

            <div style={{ textAlign: 'center' }}>
              {/* LOADER */}
              {this.state.isLoading ? <Loader color="#8637AD" size="12px" margin="4px" /> : ""}
            </div>

            {/* NOTIFICATIONS */}
            {this.state.notifications ? this.state.notifications.map((notification) => {
              return this.generateNotifications(notification)
            }) : ""}

            {/* SWEET ALERTS */}
            <SweetAlert
              warning
              show={this.state.dateAlert}
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ dateAlert: false })}>
              Please ensure you have selected dates and venue for the meet!
            </SweetAlert>

            {/* SWEET ALERTS */}
            <SweetAlert
              show={this.state.swapLoading}
              title=" Please wait!"
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="loadingBtnDisplay"
            >
              <img className="img-circle" src='assets/img/spin.gif' />
            </SweetAlert>

            {/* sweet photo loading alert */}
            <SweetAlert
              show={this.state.loadingAlert}
              confirmBtnCssClass="deleteAlertBtnColor"
              onConfirm={() => this.setState({ loadingAlert: false })}>
              Please wait! Files/Images are uploading...
            </SweetAlert>


            {/* sweet delete alert */}
            <SweetAlert
              warning
              showCancel
              show={this.state.deleteAlert}
              confirmBtnText="Yes"
              confirmBtnBsStyle="danger"
              confirmBtnCssClass="deleteAlertBtnColor"
              cancelBtnBsStyle="danger"
              cancelBtnCssClass="deleteAlertBtnColor"
              title="Are you sure?"
              onConfirm={() => this.doDelete()}
              onCancel={() => this.cancelAlertDelete()}
            >
              All data associated with this {this.state.itemToDelete} will be deleted!
            </SweetAlert>

            {/* Documents in collection */}
            {this.state.isCollection && this.state.meets.length > 0 ? this.listMeets() : ""}

            {/* DIRECT VALUE */}
            {this.state.directValue && this.state.directValue.length > 0 ? this.makeValueCard(this.state.directValue) : ""}


            {/* FIELDS */}
            {this.state.fieldsAsArray && this.state.fieldsAsArray.length > 0 ? (<div className="col-md-12">
              <div className="card">
                {/*
                <a  onClick={()=>{this.refs.simpleDialog.show()}}><div id="addDiv" className="card-header card-header-icon" data-background-color="rose" style={{float:"right"}}>
                    <i className="material-icons">add</i>
                </div></a>*/}
                <form className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{Common.capitalizeFirstLetter(Config.adminConfig.fieldBoxName)}</h4>
                  </div>
                  {this.state.fieldsAsArray ? this.state.fieldsAsArray.map((item) => {

                    return (
                      <Fields
                        isFirestore={true}
                        parentKey={null}
                        key={item.theKey + this.state.lastSub}
                        deleteFieldAction={this.deleteFieldAction}
                        updateAction={this.updateAction}
                        theKey={item.theKey}
                        value={item.value} />)


                  }) : ""}
                  <div
                    className="text-center">

                    <Link to='meets/aquaticsID+India+associations+KSA+meets'>
                      <button onClick={this.saveEdits}
                        className="btn btn-rose btn-wd margin-bottom-10px btn-width"
                        value="Save">Save</button>
                    </Link>

                    <Link to='meets/aquaticsID+India+associations+KSA+meets'>
                      <button onClick={this.resetEdits}
                        className="btn btn-rose btn-wd margin-bottom-10px btn-width"
                        value="Cancel">Cancel</button>
                    </Link>

                  </div>

                </form>
              </div>
            </div>) : ""}


            {/* COLLECTIONS */}
            {this.state.theSubLink == null && this.state.isDocument && this.state.collections && this.state.collections.length > 0 ? (<div className="col-md-12">
              <div className="card">

                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{"Collections"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.theSubLink == null && this.state.collections ? this.state.collections.map((item) => {
                      var theLink = "/assoc/" + this.state.completePath + Config.adminConfig.urlSeparator + item;
                      return (<Link to={theLink}><a className="btn">{item}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>


                </form>
              </div>
            </div>) : ""}

            {/* ARRAYS */}
            {this.state.arrayNames ? this.state.arrayNames.map((key) => {
              return this.makeArrayCard(key)
            }) : ""}

            {/* ELEMENTS MERGED IN ARRAY */}
            {this.state.elementsInArray && this.state.elementsInArray.length > 0 ? (this.makeTableCardForElementsInArray()) : ""}

            {/* ELEMENTS */}
            {this.state.elements && this.state.elements.length > 0 ? (<div className="col-md-12">
              <div className="card">

                <form method="get" action="/" className="form-horizontal">
                  <div className="card-header card-header-text" data-background-color="rose">
                    <h4 className="card-title">{this.state.lastPathItem + "' elements"}</h4>
                  </div>
                  <br />
                  <div className="col-md-12">
                    {this.state.elements ? this.state.elements.map((item) => {
                      var theLink = "/fireadmin/" + this.state.completePath + Config.adminConfig.urlSeparatorFirestoreSubArray + item.uidOfFirebase;
                      return (<Link onClick={() => { this.showSubItems(theLink) }}><a className="btn">{item.uidOfFirebase}<div className="ripple-container"></div></a></Link>)
                    }) : ""}
                  </div>


                </form>
              </div>
            </div>) : ""}




          </div>
        </div>

        <SkyLight hideOnOverlayClicked ref="addCollectionDialog" title="">
          <span><h3 className="center-block">Add first document in collection</h3></span>
          <div className="col-md-12">
            <Notification type="success" >Looks like there are no documents in this collection. Add your first document in this collection</Notification>
          </div>

          <div className="col-sm-12" style={{ marginTop: 80 }}>
            <div className="col-sm-6">
            </div>
            <div className="col-sm-3 center-block">
              <a onClick={this.cancelAddFirstItem} className="btn btn-info center-block">Cancel</a>
            </div>


          </div>

        </SkyLight>

       
       
        <SkyLight hideOnOverlayClicked ref="simpleDialog" title="">
          <span><h3 className="center-block">Add new key</h3></span>
          <br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Name of they key</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="NAME_OF_THE_NEW_KEY" value={"name"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div><br /><br />
          <div className="card-content">
            <div className="row">
              <label className="col-sm-3 label-on-left">Value</label>
              <div className="col-sm-12">
                <Input updateAction={this.updateAction} className="" theKey="VALUE_OF_THE_NEW_KEY" value={"value"} />
              </div>
              <div className="col-sm-1">
              </div>
            </div>
          </div>
          <div className="col-sm-12 ">
            <div className="col-sm-3 ">
            </div>
            <div className="col-sm-6 center-block">
              <a onClick={this.addKey} className="btn btn-rose btn-round center-block"><i className="fa fa-save"></i>   Add key</a>
            </div>
            <div className="col-sm-3 ">
            </div>
          </div>
        </SkyLight>
      </div>
    )
  }

}
export default MeetAdmin;

