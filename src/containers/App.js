import React, { Component, PropTypes } from 'react'
import { Link, Redirect } from 'react-router'
import NavBar from '../components/NavBar'
import firebase from '../config/database'
import * as firebaseREF from 'firebase';
require("firebase/firestore");


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      num_athletes: '',
      num_coaches: '',
      num_meets: '',
    }
    this.getData = this.getData.bind(this);

  }
  //to fetch or get data from the firsbase database
  getData() {
    var _this = this;
    var userId = firebase.app.auth().currentUser.uid;
    var db = firebase.app.firestore();
    var docRef = db.collection("clubmaps").doc("user" + userId);

    docRef.get().then(doc => {
      var pathdoc = doc.data();
      db.doc(pathdoc.dbpath).get().then(snap => {
        _this.setState({
          num_athletes: snap.data().num_athletes ? snap.data().num_athletes : 0,
          num_coaches: snap.data().num_coaches ? snap.data().num_coaches : 0,
          num_meets: snap.data().num_meets ? snap.data().num_meets : 0
        });
      });
    });
  }


  componentDidMount() {
    this.getData();
    window.sidebarInit();

    //Uncomment if you want to do a edirect
    //this.props.router.push('/fireadmin/clubs+skopje+items') //Path where you want user to be redirected initialy

  }

  render() {
    return (
      <div className="content">
        <NavBar>
          <a className="navbar-brand dashboard">Dashboard</a>
          <div className="pull-right">
            <a href="http://karnatakaswimming.org" target="_blank"> <img className="img-circle" src='assets/img/ksa_logo.png' height="70" width="80" /></a>
          </div>
        </NavBar>
        <div className="row">
          {/* number of coaches */}
          <Link to = "/CoachAdmin/coaches">
          <div className="col-lg-3 col-md-6 col-sm-6">
            <div className="card card-stats">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">person</i>
              </div>
              <div className="text-align-right padding-10">
                <p className="card-category">Coaches</p>
                <h3 className="card-title">{this.state.num_coaches}</h3>
              </div>
            </div>
          </div>
          </Link>
          {/* number of coaches */}
          {/* number of athletes */}
          <Link to = "/AthleteAdmin/athletes">
          <div className="col-lg-3 col-md-6 col-sm-6">
            <div className="card card-stats">
              <div className="card-header card-header-icon" data-background-color="rose">
                <i className="material-icons">pool</i>
              </div>
              <div className="text-align-right padding-10">
                <p className="card-category">Athletes</p>
                <h3 className="card-title">{this.state.num_athletes}</h3>
              </div>
            </div>
          </div>
          </Link>
          {/* number of athletes */}
            {/* number of Meets */}
            <Link to="/meets/aquaticsID+India+associations+KSA+meets">
            <div className="col-lg-3 col-md-6 col-sm-6">
              <div className="card card-stats">
                <div className="card-header card-header-icon" data-background-color="rose">
                  <i className="material-icons">alarm</i>
                </div>
                <div className="text-align-right padding-10">
                  <p className="card-category">Meets</p>
                  <h3 className="card-title">{this.state.num_meets}</h3>
                </div>
              </div>
            </div>
          </Link>
          {/* number of Meets */}
        </div>
      </div>
    )
  }
}
export default App;
